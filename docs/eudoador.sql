--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.3
-- Dumped by pg_dump version 9.1.3
-- Started on 2013-07-29 04:13:25

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 193 (class 3079 OID 11639)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2082 (class 0 OID 0)
-- Dependencies: 193
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 162 (class 1259 OID 1200320)
-- Dependencies: 5
-- Name: alerta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE alerta (
    al_cod integer NOT NULL,
    tal_id integer,
    u_id integer
);


ALTER TABLE public.alerta OWNER TO postgres;

--
-- TOC entry 161 (class 1259 OID 1200318)
-- Dependencies: 5 162
-- Name: alerta_al_cod_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE alerta_al_cod_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alerta_al_cod_seq OWNER TO postgres;

--
-- TOC entry 2083 (class 0 OID 0)
-- Dependencies: 161
-- Name: alerta_al_cod_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE alerta_al_cod_seq OWNED BY alerta.al_cod;


--
-- TOC entry 2084 (class 0 OID 0)
-- Dependencies: 161
-- Name: alerta_al_cod_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('alerta_al_cod_seq', 1, false);


--
-- TOC entry 190 (class 1259 OID 1208569)
-- Dependencies: 5
-- Name: contato; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE contato (
    con_id integer NOT NULL,
    con_nome character varying(50),
    con_assunto character varying(50),
    con_msg character varying(50000),
    con_ip character varying(50),
    con_timestamp timestamp without time zone,
    con_uid integer,
    con_email character varying(50),
    con_status integer
);


ALTER TABLE public.contato OWNER TO postgres;

--
-- TOC entry 2085 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE contato; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE contato IS '1 = não lida
0 = lida
-1 = lixeira';


--
-- TOC entry 189 (class 1259 OID 1208567)
-- Dependencies: 5 190
-- Name: contato_con_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contato_con_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contato_con_id_seq OWNER TO postgres;

--
-- TOC entry 2086 (class 0 OID 0)
-- Dependencies: 189
-- Name: contato_con_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contato_con_id_seq OWNED BY contato.con_id;


--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 189
-- Name: contato_con_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contato_con_id_seq', 3, true);


--
-- TOC entry 164 (class 1259 OID 1200331)
-- Dependencies: 1954 5
-- Name: denuncia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE denuncia (
    den_id integer NOT NULL,
    u_id integer,
    u_id_denunciado integer,
    un_rev_id integer,
    tden_cod integer,
    den_timestamp timestamp without time zone DEFAULT now(),
    den_situacao integer,
    den_text text
);


ALTER TABLE public.denuncia OWNER TO postgres;

--
-- TOC entry 163 (class 1259 OID 1200329)
-- Dependencies: 5 164
-- Name: denuncia_den_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE denuncia_den_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.denuncia_den_id_seq OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 163
-- Name: denuncia_den_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE denuncia_den_id_seq OWNED BY denuncia.den_id;


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 163
-- Name: denuncia_den_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('denuncia_den_id_seq', 2, true);


--
-- TOC entry 165 (class 1259 OID 1200346)
-- Dependencies: 5
-- Name: facebook; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE facebook (
    fb_id bigint NOT NULL,
    fb_firstname character varying(80),
    fb_name character varying(120),
    fb_username character varying(50),
    fb_sexo character varying(10),
    fb_link character varying(150),
    fb_email character varying(50),
    fb_locale character varying(15)
);


ALTER TABLE public.facebook OWNER TO postgres;

--
-- TOC entry 167 (class 1259 OID 1200354)
-- Dependencies: 5
-- Name: log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE log (
    log_id integer NOT NULL,
    u_id integer,
    log_nivel integer,
    log_timestamp timestamp without time zone,
    log_text text
);


ALTER TABLE public.log OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 1200352)
-- Dependencies: 5 167
-- Name: log_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_log_id_seq OWNER TO postgres;

--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 166
-- Name: log_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_log_id_seq OWNED BY log.log_id;


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 166
-- Name: log_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_log_id_seq', 11, true);


--
-- TOC entry 168 (class 1259 OID 1200365)
-- Dependencies: 5
-- Name: pessoa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pessoa (
    u_id integer NOT NULL,
    pes_nome character varying(150),
    pes_apelido character varying(100),
    pes_rg character varying(20),
    pes_cpf character varying(11),
    pes_sexo character varying(10),
    pes_uf character varying(2),
    pes_cidade character varying(450),
    pes_lat character varying(100),
    pes_lon character varying(100)
);


ALTER TABLE public.pessoa OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 1200373)
-- Dependencies: 1957 5
-- Name: soldoador; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE soldoador (
    sd_id integer NOT NULL,
    u_id integer,
    sol_id integer,
    sd_timestamp timestamp without time zone DEFAULT now()
);


ALTER TABLE public.soldoador OWNER TO postgres;

--
-- TOC entry 169 (class 1259 OID 1200371)
-- Dependencies: 170 5
-- Name: soldoador_sd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE soldoador_sd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.soldoador_sd_id_seq OWNER TO postgres;

--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 169
-- Name: soldoador_sd_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE soldoador_sd_id_seq OWNED BY soldoador.sd_id;


--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 169
-- Name: soldoador_sd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('soldoador_sd_id_seq', 1, false);


--
-- TOC entry 172 (class 1259 OID 1200385)
-- Dependencies: 1959 5
-- Name: solicitacao; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE solicitacao (
    sol_id integer NOT NULL,
    u_id integer,
    tsol_cod integer,
    ts_cod integer,
    un_id integer,
    sol_dt timestamp without time zone DEFAULT now(),
    sol_qtd integer,
    sol_paciente character varying(150),
    sol_situacao integer,
    sol_detalhes text
);


ALTER TABLE public.solicitacao OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 1200383)
-- Dependencies: 172 5
-- Name: solicitacao_sol_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE solicitacao_sol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitacao_sol_id_seq OWNER TO postgres;

--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 171
-- Name: solicitacao_sol_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE solicitacao_sol_id_seq OWNED BY solicitacao.sol_id;


--
-- TOC entry 2095 (class 0 OID 0)
-- Dependencies: 171
-- Name: solicitacao_sol_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('solicitacao_sol_id_seq', 5, true);


--
-- TOC entry 174 (class 1259 OID 1200399)
-- Dependencies: 5
-- Name: solshare; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE solshare (
    share_id integer NOT NULL,
    u_id integer,
    sol_id integer,
    share_dt date
);


ALTER TABLE public.solshare OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 1200397)
-- Dependencies: 174 5
-- Name: solshare_share_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE solshare_share_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solshare_share_id_seq OWNER TO postgres;

--
-- TOC entry 2096 (class 0 OID 0)
-- Dependencies: 173
-- Name: solshare_share_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE solshare_share_id_seq OWNED BY solshare.share_id;


--
-- TOC entry 2097 (class 0 OID 0)
-- Dependencies: 173
-- Name: solshare_share_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('solshare_share_id_seq', 1, false);


--
-- TOC entry 176 (class 1259 OID 1200410)
-- Dependencies: 5
-- Name: tipoalerta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipoalerta (
    tal_id integer NOT NULL,
    tal_nome character varying(50),
    tal_desc character varying(1024)
);


ALTER TABLE public.tipoalerta OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 1200408)
-- Dependencies: 176 5
-- Name: tipoalerta_tal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipoalerta_tal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipoalerta_tal_id_seq OWNER TO postgres;

--
-- TOC entry 2098 (class 0 OID 0)
-- Dependencies: 175
-- Name: tipoalerta_tal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipoalerta_tal_id_seq OWNED BY tipoalerta.tal_id;


--
-- TOC entry 2099 (class 0 OID 0)
-- Dependencies: 175
-- Name: tipoalerta_tal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipoalerta_tal_id_seq', 1, false);


--
-- TOC entry 177 (class 1259 OID 1200420)
-- Dependencies: 5
-- Name: tipodenuncia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipodenuncia (
    tden_cod integer NOT NULL,
    tden_nome character varying(50),
    tden_desc character varying(1024)
);


ALTER TABLE public.tipodenuncia OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 1200452)
-- Dependencies: 5
-- Name: tiposangue; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tiposangue (
    ts_cod integer NOT NULL,
    ts_nome character varying(25),
    ts_desc character varying(150)
);


ALTER TABLE public.tiposangue OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 1200431)
-- Dependencies: 5
-- Name: tiposolicitacao; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tiposolicitacao (
    tsol_cod integer NOT NULL,
    tsol_nome character varying(150)
);


ALTER TABLE public.tiposolicitacao OWNER TO postgres;

--
-- TOC entry 2100 (class 0 OID 0)
-- Dependencies: 179
-- Name: TABLE tiposolicitacao; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tiposolicitacao IS '-- direcionada a paciente
-- reposicao de bancos de sangue';


--
-- TOC entry 178 (class 1259 OID 1200429)
-- Dependencies: 5 179
-- Name: tiposolicitacao_tsol_cod_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tiposolicitacao_tsol_cod_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tiposolicitacao_tsol_cod_seq OWNER TO postgres;

--
-- TOC entry 2101 (class 0 OID 0)
-- Dependencies: 178
-- Name: tiposolicitacao_tsol_cod_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tiposolicitacao_tsol_cod_seq OWNED BY tiposolicitacao.tsol_cod;


--
-- TOC entry 2102 (class 0 OID 0)
-- Dependencies: 178
-- Name: tiposolicitacao_tsol_cod_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tiposolicitacao_tsol_cod_seq', 1, false);


--
-- TOC entry 181 (class 1259 OID 1200440)
-- Dependencies: 5
-- Name: tipounidade; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipounidade (
    tun_id integer NOT NULL,
    tun_nome character varying(50),
    tun_desc character varying(450),
    tun_param integer
);


ALTER TABLE public.tipounidade OWNER TO postgres;

--
-- TOC entry 2103 (class 0 OID 0)
-- Dependencies: 181
-- Name: TABLE tipounidade; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tipounidade IS '-- Hemocentro
-- Hemonúcleo
-- Hospital
-- Unidade de coleta e transfusão
-- Unidade de transfusão';


--
-- TOC entry 180 (class 1259 OID 1200438)
-- Dependencies: 5 181
-- Name: tipounidade_tun_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipounidade_tun_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipounidade_tun_id_seq OWNER TO postgres;

--
-- TOC entry 2104 (class 0 OID 0)
-- Dependencies: 180
-- Name: tipounidade_tun_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipounidade_tun_id_seq OWNED BY tipounidade.tun_id;


--
-- TOC entry 2105 (class 0 OID 0)
-- Dependencies: 180
-- Name: tipounidade_tun_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipounidade_tun_id_seq', 5, true);


--
-- TOC entry 182 (class 1259 OID 1200450)
-- Dependencies: 5 183
-- Name: ttiposangue_ts_cod_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ttiposangue_ts_cod_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ttiposangue_ts_cod_seq OWNER TO postgres;

--
-- TOC entry 2106 (class 0 OID 0)
-- Dependencies: 182
-- Name: ttiposangue_ts_cod_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ttiposangue_ts_cod_seq OWNED BY tiposangue.ts_cod;


--
-- TOC entry 2107 (class 0 OID 0)
-- Dependencies: 182
-- Name: ttiposangue_ts_cod_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ttiposangue_ts_cod_seq', 10, true);


--
-- TOC entry 185 (class 1259 OID 1200461)
-- Dependencies: 5
-- Name: unidade; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE unidade (
    un_id integer NOT NULL,
    un_situacao integer,
    un_modificacoes integer
);


ALTER TABLE public.unidade OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 1200459)
-- Dependencies: 185 5
-- Name: unidade_un_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE unidade_un_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidade_un_id_seq OWNER TO postgres;

--
-- TOC entry 2108 (class 0 OID 0)
-- Dependencies: 184
-- Name: unidade_un_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE unidade_un_id_seq OWNED BY unidade.un_id;


--
-- TOC entry 2109 (class 0 OID 0)
-- Dependencies: 184
-- Name: unidade_un_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('unidade_un_id_seq', 6, true);


--
-- TOC entry 186 (class 1259 OID 1200468)
-- Dependencies: 1966 5
-- Name: unidaderev; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE unidaderev (
    un_id integer NOT NULL,
    u_id integer,
    tun_id integer,
    rev_timestamp timestamp without time zone DEFAULT now(),
    rev_nregistro character varying(30),
    rev_cnpj character varying(20),
    rev_nome character varying(450),
    rev_desc character varying(450),
    rev_uf character varying(450),
    rev_cidade character varying(450),
    rev_cep character varying(450),
    rev_rua character varying(450),
    rev_num character varying(450),
    rev_obs character varying(450),
    rev_lat character varying(450),
    rev_lon character varying(450),
    rev_id integer NOT NULL,
    rev_bairro character varying(450)
);


ALTER TABLE public.unidaderev OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 1208628)
-- Dependencies: 186 5
-- Name: unidaderev_rev_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE unidaderev_rev_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidaderev_rev_id_seq OWNER TO postgres;

--
-- TOC entry 2110 (class 0 OID 0)
-- Dependencies: 191
-- Name: unidaderev_rev_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE unidaderev_rev_id_seq OWNED BY unidaderev.rev_id;


--
-- TOC entry 2111 (class 0 OID 0)
-- Dependencies: 191
-- Name: unidaderev_rev_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('unidaderev_rev_id_seq', 7, true);


--
-- TOC entry 188 (class 1259 OID 1200483)
-- Dependencies: 1969 5
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    u_id integer NOT NULL,
    fb_id bigint,
    u_email character varying(100),
    u_senha character varying(128),
    u_dt_cad timestamp without time zone DEFAULT now(),
    u_situacao integer,
    u_tipo integer,
    u_hash character varying(32)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE usuario; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE usuario IS ' u_tipo: 
1 : Ativo, com email verificado
0 : Ativo, sem email verificado
-1: Inativo';


--
-- TOC entry 187 (class 1259 OID 1200481)
-- Dependencies: 188 5
-- Name: usuario_u_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_u_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_u_id_seq OWNER TO postgres;

--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 187
-- Name: usuario_u_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_u_id_seq OWNED BY usuario.u_id;


--
-- TOC entry 2114 (class 0 OID 0)
-- Dependencies: 187
-- Name: usuario_u_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_u_id_seq', 54, true);


--
-- TOC entry 192 (class 1259 OID 1208720)
-- Dependencies: 1951 5
-- Name: v_unidades; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_unidades AS
    SELECT un.un_id, un.un_situacao, un.un_modificacoes, rev.rev_id, rev.u_id, rev.tun_id, rev.rev_timestamp, rev.rev_nregistro, rev.rev_cnpj, rev.rev_nome, rev.rev_desc, rev.rev_uf, rev.rev_cidade, rev.rev_cep, rev.rev_bairro, rev.rev_rua, rev.rev_num, rev.rev_obs, rev.rev_lat, rev.rev_lon FROM (unidade un JOIN unidaderev rev ON ((rev.un_id = un.un_id))) GROUP BY un.un_id, un.un_situacao, un.un_modificacoes, rev.rev_id, rev.u_id, rev.tun_id, rev.rev_timestamp, rev.rev_nregistro, rev.rev_cnpj, rev.rev_nome, rev.rev_desc, rev.rev_uf, rev.rev_cidade, rev.rev_cep, rev.rev_bairro, rev.rev_rua, rev.rev_num, rev.rev_obs, rev.rev_lat, rev.rev_lon HAVING (rev.rev_id = (SELECT max(unidaderev.rev_id) AS max FROM unidaderev WHERE (unidaderev.un_id = un.un_id)));


ALTER TABLE public.v_unidades OWNER TO postgres;

--
-- TOC entry 1952 (class 2604 OID 1200323)
-- Dependencies: 162 161 162
-- Name: al_cod; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alerta ALTER COLUMN al_cod SET DEFAULT nextval('alerta_al_cod_seq'::regclass);


--
-- TOC entry 1970 (class 2604 OID 1208572)
-- Dependencies: 190 189 190
-- Name: con_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contato ALTER COLUMN con_id SET DEFAULT nextval('contato_con_id_seq'::regclass);


--
-- TOC entry 1953 (class 2604 OID 1200334)
-- Dependencies: 164 163 164
-- Name: den_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY denuncia ALTER COLUMN den_id SET DEFAULT nextval('denuncia_den_id_seq'::regclass);


--
-- TOC entry 1955 (class 2604 OID 1200357)
-- Dependencies: 167 166 167
-- Name: log_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log ALTER COLUMN log_id SET DEFAULT nextval('log_log_id_seq'::regclass);


--
-- TOC entry 1956 (class 2604 OID 1200376)
-- Dependencies: 169 170 170
-- Name: sd_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY soldoador ALTER COLUMN sd_id SET DEFAULT nextval('soldoador_sd_id_seq'::regclass);


--
-- TOC entry 1958 (class 2604 OID 1200388)
-- Dependencies: 172 171 172
-- Name: sol_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitacao ALTER COLUMN sol_id SET DEFAULT nextval('solicitacao_sol_id_seq'::regclass);


--
-- TOC entry 1960 (class 2604 OID 1200402)
-- Dependencies: 173 174 174
-- Name: share_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solshare ALTER COLUMN share_id SET DEFAULT nextval('solshare_share_id_seq'::regclass);


--
-- TOC entry 1961 (class 2604 OID 1200413)
-- Dependencies: 176 175 176
-- Name: tal_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipoalerta ALTER COLUMN tal_id SET DEFAULT nextval('tipoalerta_tal_id_seq'::regclass);


--
-- TOC entry 1964 (class 2604 OID 1200455)
-- Dependencies: 182 183 183
-- Name: ts_cod; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tiposangue ALTER COLUMN ts_cod SET DEFAULT nextval('ttiposangue_ts_cod_seq'::regclass);


--
-- TOC entry 1962 (class 2604 OID 1200434)
-- Dependencies: 178 179 179
-- Name: tsol_cod; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tiposolicitacao ALTER COLUMN tsol_cod SET DEFAULT nextval('tiposolicitacao_tsol_cod_seq'::regclass);


--
-- TOC entry 1963 (class 2604 OID 1200443)
-- Dependencies: 181 180 181
-- Name: tun_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipounidade ALTER COLUMN tun_id SET DEFAULT nextval('tipounidade_tun_id_seq'::regclass);


--
-- TOC entry 1965 (class 2604 OID 1200464)
-- Dependencies: 185 184 185
-- Name: un_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY unidade ALTER COLUMN un_id SET DEFAULT nextval('unidade_un_id_seq'::regclass);


--
-- TOC entry 1967 (class 2604 OID 1208630)
-- Dependencies: 191 186
-- Name: rev_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY unidaderev ALTER COLUMN rev_id SET DEFAULT nextval('unidaderev_rev_id_seq'::regclass);


--
-- TOC entry 1968 (class 2604 OID 1200486)
-- Dependencies: 188 187 188
-- Name: u_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN u_id SET DEFAULT nextval('usuario_u_id_seq'::regclass);


--
-- TOC entry 2060 (class 0 OID 1200320)
-- Dependencies: 162
-- Data for Name: alerta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY alerta (al_cod, tal_id, u_id) FROM stdin;
\.


--
-- TOC entry 2076 (class 0 OID 1208569)
-- Dependencies: 190
-- Data for Name: contato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY contato (con_id, con_nome, con_assunto, con_msg, con_ip, con_timestamp, con_uid, con_email, con_status) FROM stdin;
2	Leonardo Nicorena	Teste	HÁ!!!	192.168.1.1	2013-07-26 15:02:34.964	\N	leonardo_nicorena@hotmail.com	0
3	Leonardo Nicorena	Teste	HÁ!!!	127.0.0.1	2013-07-26 15:03:14.63	54	leonardo_nicorena@hotmail.com	0
\.


--
-- TOC entry 2061 (class 0 OID 1200331)
-- Dependencies: 164
-- Data for Name: denuncia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY denuncia (den_id, u_id, u_id_denunciado, un_rev_id, tden_cod, den_timestamp, den_situacao, den_text) FROM stdin;
2	54	54	3	1	2013-07-29 00:22:47.542	0	Teste
\.


--
-- TOC entry 2062 (class 0 OID 1200346)
-- Dependencies: 165
-- Data for Name: facebook; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY facebook (fb_id, fb_firstname, fb_name, fb_username, fb_sexo, fb_link, fb_email, fb_locale) FROM stdin;
100000275598765	\N	\N	lnicorena	\N	\N	\N	\N
\.


--
-- TOC entry 2063 (class 0 OID 1200354)
-- Dependencies: 167
-- Data for Name: log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY log (log_id, u_id, log_nivel, log_timestamp, log_text) FROM stdin;
3	\N	0	2013-07-14 05:56:28.572	a:6:{i:0;s:5:"Teste";i:1;s:4:"há!";s:2:"ip";s:9:"127.0.0.1";s:6:"module";s:7:"default";s:10:"controller";s:5:"index";s:6:"action";s:5:"email";}
6	54	0	2013-07-25 11:53:11.214	a:6:{i:0;s:6:"Teste2";i:1;s:26:"Exemplo de mensagem de log";s:2:"ip";s:9:"127.0.0.1";s:6:"module";s:7:"default";s:10:"controller";s:5:"index";s:6:"action";s:5:"email";}
7	54	0	2013-07-27 21:12:27.207	a:7:{i:0;s:17:"Unidade inserida.";i:1;s:7:"un_id:5";i:2;s:27:"usuario:54Leonardo Nicorena";s:2:"ip";s:9:"127.0.0.1";s:6:"module";s:7:"default";s:10:"controller";s:7:"unidade";s:6:"action";s:3:"add";}
8	54	0	2013-07-28 04:50:36.16	a:7:{i:0;s:17:"Unidade inserida.";i:1;s:7:"un_id:6";i:2;s:27:"usuario:54Leonardo Nicorena";s:2:"ip";s:9:"127.0.0.1";s:6:"module";s:7:"default";s:10:"controller";s:7:"unidade";s:6:"action";s:3:"add";}
9	54	0	2013-07-29 00:48:04.272	a:7:{i:0;s:17:"Unidade inserida.";i:1;s:7:"un_id:5";i:2;s:27:"usuario:54Leonardo Nicorena";s:2:"ip";s:9:"127.0.0.1";s:6:"module";s:7:"default";s:10:"controller";s:7:"unidade";s:6:"action";s:6:"editar";}
10	54	0	2013-07-29 00:52:25.323	a:7:{i:0;s:17:"Unidade inserida.";i:1;s:7:"un_id:5";i:2;s:27:"usuario:54Leonardo Nicorena";s:2:"ip";s:9:"127.0.0.1";s:6:"module";s:7:"default";s:10:"controller";s:7:"unidade";s:6:"action";s:6:"editar";}
11	54	0	2013-07-29 02:43:53.298	a:7:{i:0;s:24:"Solicitação realizada.";i:1;s:27:"usuario:54Leonardo Nicorena";i:2;a:8:{s:6:"sol_id";s:1:"0";s:5:"un_id";s:1:"6";s:8:"tsol_cod";s:1:"1";s:6:"ts_cod";s:1:"4";s:7:"sol_qtd";s:0:"";s:12:"sol_paciente";s:6:"adasda";s:12:"sol_detalhes";s:0:"";s:6:"submit";s:8:"Concluir";}s:2:"ip";s:9:"127.0.0.1";s:6:"module";s:7:"default";s:10:"controller";s:11:"solicitacao";s:6:"action";s:4:"novo";}
\.


--
-- TOC entry 2064 (class 0 OID 1200365)
-- Dependencies: 168
-- Data for Name: pessoa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pessoa (u_id, pes_nome, pes_apelido, pes_rg, pes_cpf, pes_sexo, pes_uf, pes_cidade, pes_lat, pes_lon) FROM stdin;
1	Leonardo Nicorena	Leonardo	\N	\N	M	\N	\N	\N	\N
30	Leonardo Nicorena	lnicorena	1231	12312	M	RS	SM	\N	\N
31	Leonardo Nicorena	lnicorena	1231	12312	M	RS	SM	\N	\N
36	Leonardo Nicorena	lnicorena	1231	12312	M	RS	SM	\N	\N
54	Leonardo Nicorena	Leonardo	\N	\N	M	RS	SM	\N	\N
\.


--
-- TOC entry 2065 (class 0 OID 1200373)
-- Dependencies: 170
-- Data for Name: soldoador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY soldoador (sd_id, u_id, sol_id, sd_timestamp) FROM stdin;
\.


--
-- TOC entry 2066 (class 0 OID 1200385)
-- Dependencies: 172
-- Data for Name: solicitacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY solicitacao (sol_id, u_id, tsol_cod, ts_cod, un_id, sol_dt, sol_qtd, sol_paciente, sol_situacao, sol_detalhes) FROM stdin;
4	54	1	4	6	2013-07-29 02:41:09.666	0	adasda	1	\N
5	54	1	4	6	2013-07-29 02:43:53.216	0	adasda	1	\N
\.


--
-- TOC entry 2067 (class 0 OID 1200399)
-- Dependencies: 174
-- Data for Name: solshare; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY solshare (share_id, u_id, sol_id, share_dt) FROM stdin;
\.


--
-- TOC entry 2068 (class 0 OID 1200410)
-- Dependencies: 176
-- Data for Name: tipoalerta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipoalerta (tal_id, tal_nome, tal_desc) FROM stdin;
\.


--
-- TOC entry 2069 (class 0 OID 1200420)
-- Dependencies: 177
-- Data for Name: tipodenuncia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipodenuncia (tden_cod, tden_nome, tden_desc) FROM stdin;
1	Informações incorretas	Dados incorretos inseridos nas informações da unidade
2	Conteúdo inadequado	Conteúdo impróprio inserido nas infromações da unidade (palavrões, racismo, pornografia, etc)
3	Conduta inadequada	Usuário com conduta inadequada 
4	Outros	\N
\.


--
-- TOC entry 2072 (class 0 OID 1200452)
-- Dependencies: 183
-- Data for Name: tiposangue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tiposangue (ts_cod, ts_nome, ts_desc) FROM stdin;
1	Todos	Qualquer tipo sanguíneo
2	Negativo (-)	Qualquer tipo de sanguíneo com fator Rh negarivo
3	O+	Somente sangue do tipo O+
4	O-	Somente sangue do tipo O-
5	A+	Somente sangue do tipo A+
6	A-	Somente sangue do tipo A-
7	B+	Somente sangue do tipo B+
8	B-	Somente sangue do tipo B-
9	AB+	Somente sangue do tipo AB+
10	AB-	Somente sangue do tipo AB-
\.


--
-- TOC entry 2070 (class 0 OID 1200431)
-- Dependencies: 179
-- Data for Name: tiposolicitacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tiposolicitacao (tsol_cod, tsol_nome) FROM stdin;
1	Direcionada a paciente
2	Reposição de banco de sangue
\.


--
-- TOC entry 2071 (class 0 OID 1200440)
-- Dependencies: 181
-- Data for Name: tipounidade; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipounidade (tun_id, tun_nome, tun_desc, tun_param) FROM stdin;
4	Un. de coleta e transf.	Unidade de coleta e transfusão	\N
5	Un. de transf.	Unidade de transfusão	\N
3	Hospital	Hospital	\N
2	Hemonúcleo	Hemonúcleo	\N
1	Hemocentro	Hemocentro	\N
\.


--
-- TOC entry 2073 (class 0 OID 1200461)
-- Dependencies: 185
-- Data for Name: unidade; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY unidade (un_id, un_situacao, un_modificacoes) FROM stdin;
6	1	1
5	1	3
\.


--
-- TOC entry 2074 (class 0 OID 1200468)
-- Dependencies: 186
-- Data for Name: unidaderev; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY unidaderev (un_id, u_id, tun_id, rev_timestamp, rev_nregistro, rev_cnpj, rev_nome, rev_desc, rev_uf, rev_cidade, rev_cep, rev_rua, rev_num, rev_obs, rev_lat, rev_lon, rev_id, rev_bairro) FROM stdin;
5	54	4	\N	123123	43534654	Hemocentro Regional de Santa Maria	Hemocentro Regional de Santa Maria	RS	Santa Maria	97010-004	Venâncio Aires	2092	Centro	\N	\N	3	\N
5	54	4	\N	123123	43534654	Hemocentro Regional de Santa Maria	Descrição da unidade Hemocentro Regional de Santa Maria	RS	Santa Maria	97010-004	Venâncio Aires	2092	Centro	\N	\N	6	\N
5	54	4	\N	123123	43534654	Hemocentro Regional de Santa Maria	Descrição da unidade Hemocentro Regional de Santa Maria	RS	Santa Maria	97010-004	Venâncio Aires	2092		\N	\N	7	Centro
6	54	5	\N	1231	123123	Hospital de Caridade	asdasDasd	RS	Uruguaiana	123123	asdasd	123	asasdasd	\N	\N	4	\N
\.


--
-- TOC entry 2075 (class 0 OID 1200483)
-- Dependencies: 188
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuario (u_id, fb_id, u_email, u_senha, u_dt_cad, u_situacao, u_tipo, u_hash) FROM stdin;
1	\N	admin	f595ef8e70af0537e1e13cc811a58f55	2013-07-12 03:30:54.813	1	777	\N
30	\N	lnicorena@inf.ufsm.br	202cb962ac59075b964b07152d234b70	2013-07-14 00:22:04.695	1	1	\N
31	\N	lnicorena@inf.ufsm.br	202cb962ac59075b964b07152d234b70	2013-07-14 00:25:12.918	1	1	\N
36	\N	lnicorena@inf.ufsm.br	202cb962ac59075b964b07152d234b70	2013-07-14 00:28:17.705	1	1	\N
54	100000275598765	leonardo_nicorena@hotmail.com	\N	2013-07-15 04:31:07.034	1	777	\N
\.


--
-- TOC entry 2029 (class 2606 OID 1208656)
-- Dependencies: 186 186
-- Name: fk_unidaderev; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY unidaderev
    ADD CONSTRAINT fk_unidaderev PRIMARY KEY (rev_id);


--
-- TOC entry 1975 (class 2606 OID 1200325)
-- Dependencies: 162 162
-- Name: pk_alerta; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY alerta
    ADD CONSTRAINT pk_alerta PRIMARY KEY (al_cod);


--
-- TOC entry 2038 (class 2606 OID 1208577)
-- Dependencies: 190 190
-- Name: pk_contato; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY contato
    ADD CONSTRAINT pk_contato PRIMARY KEY (con_id);


--
-- TOC entry 1982 (class 2606 OID 1200340)
-- Dependencies: 164 164
-- Name: pk_denuncia; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY denuncia
    ADD CONSTRAINT pk_denuncia PRIMARY KEY (den_id);


--
-- TOC entry 1985 (class 2606 OID 1208538)
-- Dependencies: 165 165
-- Name: pk_facebook; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY facebook
    ADD CONSTRAINT pk_facebook PRIMARY KEY (fb_id);


--
-- TOC entry 1988 (class 2606 OID 1200362)
-- Dependencies: 167 167
-- Name: pk_log; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY log
    ADD CONSTRAINT pk_log PRIMARY KEY (log_id);


--
-- TOC entry 1992 (class 2606 OID 1200369)
-- Dependencies: 168 168
-- Name: pk_pessoa; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pessoa
    ADD CONSTRAINT pk_pessoa PRIMARY KEY (u_id);


--
-- TOC entry 2006 (class 2606 OID 1200404)
-- Dependencies: 174 174
-- Name: pk_solshare; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY solshare
    ADD CONSTRAINT pk_solshare PRIMARY KEY (share_id);


--
-- TOC entry 1994 (class 2606 OID 1200379)
-- Dependencies: 170 170
-- Name: pk_tb_sol_doado; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY soldoador
    ADD CONSTRAINT pk_tb_sol_doado PRIMARY KEY (sd_id);


--
-- TOC entry 1999 (class 2606 OID 1200391)
-- Dependencies: 172 172
-- Name: pk_tb_solicitac; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY solicitacao
    ADD CONSTRAINT pk_tb_solicitac PRIMARY KEY (sol_id);


--
-- TOC entry 2011 (class 2606 OID 1200418)
-- Dependencies: 176 176
-- Name: pk_tba_tipo_ale; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipoalerta
    ADD CONSTRAINT pk_tba_tipo_ale PRIMARY KEY (tal_id);


--
-- TOC entry 2014 (class 2606 OID 1200427)
-- Dependencies: 177 177
-- Name: pk_tba_tipo_den; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipodenuncia
    ADD CONSTRAINT pk_tba_tipo_den PRIMARY KEY (tden_cod);


--
-- TOC entry 2023 (class 2606 OID 1200457)
-- Dependencies: 183 183
-- Name: pk_tba_tipo_san; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tiposangue
    ADD CONSTRAINT pk_tba_tipo_san PRIMARY KEY (ts_cod);


--
-- TOC entry 2017 (class 2606 OID 1200436)
-- Dependencies: 179 179
-- Name: pk_tba_tipo_sol; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tiposolicitacao
    ADD CONSTRAINT pk_tba_tipo_sol PRIMARY KEY (tsol_cod);


--
-- TOC entry 2020 (class 2606 OID 1200448)
-- Dependencies: 181 181
-- Name: pk_tba_tipo_uni; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipounidade
    ADD CONSTRAINT pk_tba_tipo_uni PRIMARY KEY (tun_id);


--
-- TOC entry 2026 (class 2606 OID 1200466)
-- Dependencies: 185 185
-- Name: pk_unidade; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY unidade
    ADD CONSTRAINT pk_unidade PRIMARY KEY (un_id);


--
-- TOC entry 2034 (class 2606 OID 1200489)
-- Dependencies: 188 188
-- Name: pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (u_id);


--
-- TOC entry 1971 (class 1259 OID 1200326)
-- Dependencies: 162
-- Name: alerta_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX alerta_pk ON alerta USING btree (al_cod);


--
-- TOC entry 1972 (class 1259 OID 1200327)
-- Dependencies: 162
-- Name: alerta_talerta_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX alerta_talerta_fk ON alerta USING btree (tal_id);


--
-- TOC entry 1973 (class 1259 OID 1200328)
-- Dependencies: 162
-- Name: alerta_usu_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX alerta_usu_fk ON alerta USING btree (u_id);


--
-- TOC entry 1976 (class 1259 OID 1200344)
-- Dependencies: 164
-- Name: den_rev_unid_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX den_rev_unid_fk ON denuncia USING btree (un_rev_id);


--
-- TOC entry 1977 (class 1259 OID 1200345)
-- Dependencies: 164
-- Name: den_tdenuncia_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX den_tdenuncia_fk ON denuncia USING btree (tden_cod);


--
-- TOC entry 1978 (class 1259 OID 1200343)
-- Dependencies: 164
-- Name: den_uacusado_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX den_uacusado_fk ON denuncia USING btree (u_id_denunciado);


--
-- TOC entry 1979 (class 1259 OID 1200342)
-- Dependencies: 164
-- Name: den_udenunciante_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX den_udenunciante_fk ON denuncia USING btree (u_id);


--
-- TOC entry 1980 (class 1259 OID 1200341)
-- Dependencies: 164
-- Name: denuncia_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX denuncia_pk ON denuncia USING btree (den_id);


--
-- TOC entry 1983 (class 1259 OID 1208536)
-- Dependencies: 165
-- Name: facebook_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX facebook_pk ON facebook USING btree (fb_id);


--
-- TOC entry 1986 (class 1259 OID 1200363)
-- Dependencies: 167
-- Name: log_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX log_pk ON log USING btree (log_id);


--
-- TOC entry 1990 (class 1259 OID 1200370)
-- Dependencies: 168
-- Name: pessoa_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX pessoa_pk ON pessoa USING btree (u_id);


--
-- TOC entry 2007 (class 1259 OID 1200407)
-- Dependencies: 174
-- Name: share_solic_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX share_solic_fk ON solshare USING btree (sol_id);


--
-- TOC entry 2008 (class 1259 OID 1200406)
-- Dependencies: 174
-- Name: share_usu_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX share_usu_fk ON solshare USING btree (u_id);


--
-- TOC entry 1995 (class 1259 OID 1200380)
-- Dependencies: 170
-- Name: soldoador_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX soldoador_pk ON soldoador USING btree (sd_id);


--
-- TOC entry 2000 (class 1259 OID 1200395)
-- Dependencies: 172
-- Name: solic_tsangue_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX solic_tsangue_fk ON solicitacao USING btree (ts_cod);


--
-- TOC entry 2001 (class 1259 OID 1200394)
-- Dependencies: 172
-- Name: solic_tsolic_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX solic_tsolic_fk ON solicitacao USING btree (tsol_cod);


--
-- TOC entry 1996 (class 1259 OID 1200382)
-- Dependencies: 170
-- Name: solicdoador_solic_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX solicdoador_solic_fk ON soldoador USING btree (sol_id);


--
-- TOC entry 1997 (class 1259 OID 1200381)
-- Dependencies: 170
-- Name: solicdoador_usuario_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX solicdoador_usuario_fk ON soldoador USING btree (u_id);


--
-- TOC entry 2002 (class 1259 OID 1200392)
-- Dependencies: 172
-- Name: solicitacao_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX solicitacao_pk ON solicitacao USING btree (sol_id);


--
-- TOC entry 2009 (class 1259 OID 1200405)
-- Dependencies: 174
-- Name: solshare_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX solshare_pk ON solshare USING btree (share_id);


--
-- TOC entry 2012 (class 1259 OID 1200419)
-- Dependencies: 176
-- Name: tipoalerta_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX tipoalerta_pk ON tipoalerta USING btree (tal_id);


--
-- TOC entry 2015 (class 1259 OID 1200428)
-- Dependencies: 177
-- Name: tipodenuncia_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX tipodenuncia_pk ON tipodenuncia USING btree (tden_cod);


--
-- TOC entry 2018 (class 1259 OID 1200437)
-- Dependencies: 179
-- Name: tiposolicitacao_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX tiposolicitacao_pk ON tiposolicitacao USING btree (tsol_cod);


--
-- TOC entry 2021 (class 1259 OID 1200449)
-- Dependencies: 181
-- Name: tipounidade_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX tipounidade_pk ON tipounidade USING btree (tun_id);


--
-- TOC entry 2024 (class 1259 OID 1200458)
-- Dependencies: 183
-- Name: ttiposangue_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ttiposangue_pk ON tiposangue USING btree (ts_cod);


--
-- TOC entry 2030 (class 1259 OID 1200480)
-- Dependencies: 186
-- Name: un_rev_tunid_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX un_rev_tunid_fk ON unidaderev USING btree (tun_id);


--
-- TOC entry 2031 (class 1259 OID 1200479)
-- Dependencies: 186
-- Name: un_rev_unid_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX un_rev_unid_fk ON unidaderev USING btree (un_id);


--
-- TOC entry 2032 (class 1259 OID 1200478)
-- Dependencies: 186
-- Name: un_rev_usu_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX un_rev_usu_fk ON unidaderev USING btree (u_id);


--
-- TOC entry 2003 (class 1259 OID 1200396)
-- Dependencies: 172
-- Name: un_solic_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX un_solic_fk ON solicitacao USING btree (un_id);


--
-- TOC entry 2027 (class 1259 OID 1200467)
-- Dependencies: 185
-- Name: unidade_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unidade_pk ON unidade USING btree (un_id);


--
-- TOC entry 2035 (class 1259 OID 1208549)
-- Dependencies: 188
-- Name: usu_fb_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX usu_fb_fk ON usuario USING btree (fb_id);


--
-- TOC entry 1989 (class 1259 OID 1200364)
-- Dependencies: 167
-- Name: usu_log_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX usu_log_fk ON log USING btree (u_id);


--
-- TOC entry 2004 (class 1259 OID 1200393)
-- Dependencies: 172
-- Name: usu_solic_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX usu_solic_fk ON solicitacao USING btree (u_id);


--
-- TOC entry 2036 (class 1259 OID 1200490)
-- Dependencies: 188
-- Name: usuario_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX usuario_pk ON usuario USING btree (u_id);


--
-- TOC entry 2059 (class 2606 OID 1208588)
-- Dependencies: 188 190 2033
-- Name: FK_Contato_Usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contato
    ADD CONSTRAINT "FK_Contato_Usuario" FOREIGN KEY (con_uid) REFERENCES usuario(u_id) ON UPDATE CASCADE;


--
-- TOC entry 2039 (class 2606 OID 1200492)
-- Dependencies: 2010 162 176
-- Name: fk_alerta_alerta_ta_tipoaler; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alerta
    ADD CONSTRAINT fk_alerta_alerta_ta_tipoaler FOREIGN KEY (tal_id) REFERENCES tipoalerta(tal_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2040 (class 2606 OID 1200497)
-- Dependencies: 162 188 2033
-- Name: fk_alerta_alerta_us_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY alerta
    ADD CONSTRAINT fk_alerta_alerta_us_usuario FOREIGN KEY (u_id) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2041 (class 2606 OID 1208657)
-- Dependencies: 164 2013 177
-- Name: fk_denuncia_den_tdenu_tipodenu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY denuncia
    ADD CONSTRAINT fk_denuncia_den_tdenu_tipodenu FOREIGN KEY (tden_cod) REFERENCES tipodenuncia(tden_cod) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2042 (class 2606 OID 1208662)
-- Dependencies: 188 2033 164
-- Name: fk_denuncia_den_uacus_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY denuncia
    ADD CONSTRAINT fk_denuncia_den_uacus_usuario FOREIGN KEY (u_id_denunciado) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2043 (class 2606 OID 1208667)
-- Dependencies: 2033 188 164
-- Name: fk_denuncia_den_udenu_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY denuncia
    ADD CONSTRAINT fk_denuncia_den_udenu_usuario FOREIGN KEY (u_id) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2045 (class 2606 OID 1208508)
-- Dependencies: 188 167 2033
-- Name: fk_log_usu_log_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log
    ADD CONSTRAINT fk_log_usu_log_usuario FOREIGN KEY (u_id) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2046 (class 2606 OID 1208521)
-- Dependencies: 188 168 2033
-- Name: fk_pessoa_usu_pes_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pessoa
    ADD CONSTRAINT fk_pessoa_usu_pes_usuario FOREIGN KEY (u_id) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2047 (class 2606 OID 1200532)
-- Dependencies: 1998 170 172
-- Name: fk_soldoado_solicdoad_solicita; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY soldoador
    ADD CONSTRAINT fk_soldoado_solicdoad_solicita FOREIGN KEY (sol_id) REFERENCES solicitacao(sol_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2048 (class 2606 OID 1200537)
-- Dependencies: 2033 170 188
-- Name: fk_soldoado_solicdoad_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY soldoador
    ADD CONSTRAINT fk_soldoado_solicdoad_usuario FOREIGN KEY (u_id) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2049 (class 2606 OID 1208695)
-- Dependencies: 183 2022 172
-- Name: fk_solicita_solic_tsa_ttiposan; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitacao
    ADD CONSTRAINT fk_solicita_solic_tsa_ttiposan FOREIGN KEY (ts_cod) REFERENCES tiposangue(ts_cod) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2050 (class 2606 OID 1208700)
-- Dependencies: 2016 179 172
-- Name: fk_solicita_solic_tso_tiposoli; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitacao
    ADD CONSTRAINT fk_solicita_solic_tso_tiposoli FOREIGN KEY (tsol_cod) REFERENCES tiposolicitacao(tsol_cod) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2051 (class 2606 OID 1208705)
-- Dependencies: 2025 172 185
-- Name: fk_solicita_un_solic_unidade; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitacao
    ADD CONSTRAINT fk_solicita_un_solic_unidade FOREIGN KEY (un_id) REFERENCES unidade(un_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2052 (class 2606 OID 1208710)
-- Dependencies: 2033 172 188
-- Name: fk_solicita_usu_solic_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitacao
    ADD CONSTRAINT fk_solicita_usu_solic_usuario FOREIGN KEY (u_id) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2053 (class 2606 OID 1200562)
-- Dependencies: 1998 174 172
-- Name: fk_solshare_share_sol_solicita; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solshare
    ADD CONSTRAINT fk_solshare_share_sol_solicita FOREIGN KEY (sol_id) REFERENCES solicitacao(sol_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2054 (class 2606 OID 1200567)
-- Dependencies: 2033 188 174
-- Name: fk_solshare_share_usu_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solshare
    ADD CONSTRAINT fk_solshare_share_usu_usuario FOREIGN KEY (u_id) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2055 (class 2606 OID 1208677)
-- Dependencies: 2019 181 186
-- Name: fk_unidader_un_rev_tu_tipounid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY unidaderev
    ADD CONSTRAINT fk_unidader_un_rev_tu_tipounid FOREIGN KEY (tun_id) REFERENCES tipounidade(tun_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2056 (class 2606 OID 1208682)
-- Dependencies: 2025 185 186
-- Name: fk_unidader_un_rev_un_unidade; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY unidaderev
    ADD CONSTRAINT fk_unidader_un_rev_un_unidade FOREIGN KEY (un_id) REFERENCES unidade(un_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2057 (class 2606 OID 1208687)
-- Dependencies: 186 2033 188
-- Name: fk_unidader_un_rev_us_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY unidaderev
    ADD CONSTRAINT fk_unidader_un_rev_us_usuario FOREIGN KEY (u_id) REFERENCES usuario(u_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2058 (class 2606 OID 1208561)
-- Dependencies: 188 1983 165
-- Name: fk_usuario_usu_fb_facebook; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fk_usuario_usu_fb_facebook FOREIGN KEY (fb_id) REFERENCES facebook(fb_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2044 (class 2606 OID 1208672)
-- Dependencies: 164 2028 186
-- Name: fl_denuncia_den_unidaderev; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY denuncia
    ADD CONSTRAINT fl_denuncia_den_unidaderev FOREIGN KEY (un_rev_id) REFERENCES unidaderev(rev_id);


--
-- TOC entry 2081 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-07-29 04:13:27

--
-- PostgreSQL database dump complete
--

