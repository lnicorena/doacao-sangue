<?php

class Doacao_Model_Solicitacao extends Doacao_Model_ModelAbstract {

    protected $sol_id;
    protected $ts_cod;
    protected $tsol_cod;
    protected $un_id;
    protected $u_id;
    protected $sol_dt;
    protected $sol_qtd;
    protected $sol_paciente;
    protected $sol_situacao;
    protected $ttiposangue;
    protected $tiposolicitacao;
    protected $sol_detalhes;

    public function __construct($solicitacao = null) {
        if (!is_null($solicitacao) && $solicitacao instanceof Zend_Db_Table_Row) {
            $this->sol_id = $solicitacao->sol_id;
            $this->ts_cod = $solicitacao->ts_cod;
            $this->tsol_cod = $solicitacao->tsol_cod;
            $this->un_id = $solicitacao->un_id;
            $this->u_id = $solicitacao->u_id;
            $this->sol_dt = $solicitacao->sol_dt;
            $this->sol_qtd = $solicitacao->sol_qtd;
            $this->sol_paciente = $solicitacao->sol_paciente;
            $this->sol_situacao = $solicitacao->sol_situacao;
            $this->sol_detalhes = $solicitacao->sol_detalhes;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
