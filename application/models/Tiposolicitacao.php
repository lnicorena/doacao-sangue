<?php

class Doacao_Model_Tiposolicitacao extends Doacao_Model_ModelAbstract {

    protected $tsol_cod;
    protected $tsol_nome;

    public function __construct($tiposolicitacao = null) {
        if (!is_null($tiposolicitacao) && $tiposolicitacao instanceof Zend_Db_Table_Row) {
            $this->tsol_cod = $tiposolicitacao->tsol_cod;
            $this->tsol_nome = $tiposolicitacao->tsol_nome;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
