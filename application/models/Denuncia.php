<?php

class Doacao_Model_Denuncia extends Doacao_Model_ModelAbstract {

    protected $den_id;
    protected $un_rev_id;
    protected $tden_cod;
    protected $u_id_denunciado;
    protected $u_id;
    protected $den_timestamp;
    protected $den_situacao;
    protected $den_text;

    public function __construct($denuncia = null) {
        if (!is_null($denuncia) && $denuncia instanceof Zend_Db_Table_Row) {
            $this->den_id = $denuncia->den_id;
            $this->un_rev_id = $denuncia->un_rev_id;
            $this->tden_cod = $denuncia->tden_cod;
            $this->u_id_denunciado = $denuncia->u_id_denunciado;
            $this->u_id = $denuncia->u_id;
            $this->den_timestamp = $denuncia->den_timestamp;
            $this->den_situacao = $denuncia->den_situacao;
            $this->den_text = $denuncia->den_text;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
