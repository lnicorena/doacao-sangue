<?php

class Doacao_Model_Unidade extends Doacao_Model_ModelAbstract {

    protected $un_id;
    protected $un_situacao;
    protected $un_modificacoes;

    public function __construct($unidade = null) {
        if (!is_null($unidade) && $unidade instanceof Zend_Db_Table_Row) {
            $this->un_id = $unidade->un_id;
            $this->un_situacao = $unidade->un_situacao;
            $this->un_modificacoes = $unidade->un_modificacoes;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
