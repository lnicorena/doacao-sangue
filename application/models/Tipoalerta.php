<?php

class Doacao_Model_Tipoalerta extends Doacao_Model_ModelAbstract {

    protected $tal_id;
    protected $tal_nome;
    protected $tal_desc;

    public function __construct($tipoalerta = null) {
        if (!is_null($tipoalerta) && $tipoalerta instanceof Zend_Db_Table_Row) {
            $this->tal_id = $tipoalerta->tal_id;
            $this->tal_nome = $tipoalerta->tal_nome;
            $this->tal_desc = $tipoalerta->tal_desc;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
