<?php

class Doacao_Model_Alerta extends Doacao_Model_ModelAbstract {

    protected $al_cod;
    protected $tal_id;
    protected $u_id;

    public function __construct($alerta = null) {
        if (!is_null($alerta) && $alerta instanceof Zend_Db_Table_Row) {
            $this->al_cod = $alerta->al_cod;
            $this->tal_id = $alerta->tal_id;
            $this->u_id = $alerta->u_id;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
