<?php

class Doacao_Model_Solshare extends Doacao_Model_ModelAbstract {

    protected $share_id;
    protected $sol_id;
    protected $u_id;
    protected $share_dt;
    protected $sol;
    protected $u;

    public function __construct($solshare = null) {
        if (!is_null($solshare) && $solshare instanceof Zend_Db_Table_Row) {
                    $this->share_id = $solshare->share_id;
		            $this->sol_id = $solshare->sol_id;
		            $this->u_id = $solshare->u_id;
		            $this->share_dt = $solshare->share_dt;
		        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
