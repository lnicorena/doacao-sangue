<?php

class Doacao_Model_Pessoa extends Doacao_Model_ModelAbstract {

    protected $u_id;
    protected $pes_nome;
    protected $pes_apelido;
    protected $pes_rg;
    protected $pes_cpf;
    protected $pes_sexo;
    protected $pes_uf;
    protected $pes_cidade;
    protected $pes_lat;
    protected $pes_lon;

    public function __construct($pessoa = null) {
        if (!is_null($pessoa) && $pessoa instanceof Zend_Db_Table_Row) {
            $this->u_id = $pessoa->u_id;
            $this->pes_nome = $pessoa->pes_nome;
            $this->pes_apelido = $pessoa->pes_apelido;
            $this->pes_rg = $pessoa->pes_rg;
            $this->pes_cpf = $pessoa->pes_cpf;
            $this->pes_sexo = $pessoa->pes_sexo;
            $this->pes_uf = $pessoa->pes_uf;
            $this->pes_cidade = $pessoa->pes_cidade;
            $this->pes_lat = $pessoa->pes_lat;
            $this->pes_lon = $pessoa->pes_lon;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
