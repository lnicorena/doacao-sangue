<?php

class Doacao_Model_Contato extends Doacao_Model_ModelAbstract {

    protected $con_id;
    protected $con_nome;
    protected $con_email;
    protected $con_assunto;
    protected $con_msg;
    protected $con_ip;
    protected $con_timestamp;
    protected $con_uid;
    protected $con_status;

    public function __construct($contato = null) {
        if (!is_null($contato) && $contato instanceof Zend_Db_Table_Row) {
            $this->con_id = $contato->con_id;
            $this->con_nome = $contato->con_nome;
            $this->con_email = $contato->con_email;
            $this->con_assunto = $contato->con_assunto;
            $this->con_msg = $contato->con_msg;
            $this->con_ip = $contato->con_ip;
            $this->con_timestamp = $contato->con_timestamp;
            $this->con_uid = $contato->con_uid;
            $this->con_status = $contato->con_status;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
