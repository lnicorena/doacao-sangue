<?php

class Doacao_Model_Tiposangue extends Doacao_Model_ModelAbstract {

    protected $ts_cod;
    protected $ts_nome;
    protected $ts_desc;

    public function __construct($tiposangue = null) {
        if (!is_null($tiposangue) && $tiposangue instanceof Zend_Db_Table_Row) {
            $this->ts_cod = $tiposangue->ts_cod;
            $this->ts_nome = $tiposangue->ts_nome;
            $this->ts_desc = $tiposangue->ts_desc;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
