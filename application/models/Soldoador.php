<?php

class Doacao_Model_Soldoador extends Doacao_Model_ModelAbstract {

    protected $sd_id;
    protected $sol_id;
    protected $u_id;
    protected $sd_timestamp;
    protected $sol;
    protected $u;

    public function __construct($soldoador = null) {
        if (!is_null($soldoador) && $soldoador instanceof Zend_Db_Table_Row) {
                    $this->sd_id = $soldoador->sd_id;
		            $this->sol_id = $soldoador->sol_id;
		            $this->u_id = $soldoador->u_id;
		            $this->sd_timestamp = $soldoador->sd_timestamp;
		        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
