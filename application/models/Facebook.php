<?php

class Doacao_Model_Facebook extends Doacao_Model_ModelAbstract {

    protected $fb_id;
    protected $fb_firstname;
    protected $fb_name;
    protected $fb_username;
    protected $fb_sexo;
    protected $fb_link;
    protected $fb_email;
    protected $fb_locale;

    public function __construct($facebook = null) {
        if (!is_null($facebook) && $facebook instanceof Zend_Db_Table_Row) {
            $this->fb_id = $facebook->fb_id;
            $this->fb_firstname = $facebook->fb_firstname;
            $this->fb_name = $facebook->fb_name;
            $this->fb_username = $facebook->fb_username;
            $this->fb_sexo = $facebook->fb_sexo;
            $this->fb_link = $facebook->fb_link;
            $this->fb_email = $facebook->fb_email;
            $this->fb_locale = $facebook->fb_locale;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
