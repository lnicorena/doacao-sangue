<?php

class Doacao_Model_Unidaderev extends Doacao_Model_ModelAbstract {

    protected $rev_id;
    protected $tun_id;
    protected $un_id;
    protected $u_id;
    protected $rev_timestamp;
    protected $rev_nregistro;
    protected $rev_cnpj;
    protected $rev_nome;
    protected $rev_desc;
    protected $rev_uf;
    protected $rev_cidade;
    protected $rev_cep;
    protected $rev_bairro;
    protected $rev_rua;
    protected $rev_num;
    protected $rev_obs;
    protected $rev_lat;
    protected $rev_lon;

    public function __construct($unidaderev = null) {
        if (!is_null($unidaderev) && $unidaderev instanceof Zend_Db_Table_Row) {
            $this->rev_id = $unidaderev->rev_id;
            $this->tun_id = $unidaderev->tun_id;
            $this->un_id = $unidaderev->un_id;
            $this->u_id = $unidaderev->u_id;
            $this->rev_timestamp = $unidaderev->rev_timestamp;
            $this->rev_nregistro = $unidaderev->rev_nregistro;
            $this->rev_cnpj = $unidaderev->rev_cnpj;
            $this->rev_nome = $unidaderev->rev_nome;
            $this->rev_desc = $unidaderev->rev_desc;
            $this->rev_uf = $unidaderev->rev_uf;
            $this->rev_cidade = $unidaderev->rev_cidade;
            $this->rev_cep = $unidaderev->rev_cep;
            $this->rev_bairro = $unidaderev->rev_bairro;
            $this->rev_rua = $unidaderev->rev_rua;
            $this->rev_num = $unidaderev->rev_num;
            $this->rev_obs = $unidaderev->rev_obs;
            $this->rev_lat = $unidaderev->rev_lat;
            $this->rev_lon = $unidaderev->rev_lon;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
