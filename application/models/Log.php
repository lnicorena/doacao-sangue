<?php

class Doacao_Model_Log extends Doacao_Model_ModelAbstract {

    protected $log_id;
    protected $u_id;
    protected $log_nivel;
    protected $log_timestamp;
    protected $log_text;

    public function __construct($log = null) {
        if (!is_null($log) && $log instanceof Zend_Db_Table_Row) {
            $this->log_id = $log->log_id;
            $this->u_id = $log->u_id;
            $this->log_nivel = $log->log_nivel;
            $this->log_timestamp = $log->log_timestamp;
            $this->log_text = $log->log_text;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
