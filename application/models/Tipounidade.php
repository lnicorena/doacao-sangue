<?php

class Doacao_Model_Tipounidade extends Doacao_Model_ModelAbstract {

    protected $tun_id;
    protected $tun_nome;
    protected $tun_desc;
    protected $tun_param;

    public function __construct($tipounidade = null) {
        if (!is_null($tipounidade) && $tipounidade instanceof Zend_Db_Table_Row) {
            $this->tun_id = $tipounidade->tun_id;
            $this->tun_nome = $tipounidade->tun_nome;
            $this->tun_desc = $tipounidade->tun_desc;
            $this->tun_param = $tipounidade->tun_param;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
