<?php

class Doacao_Model_Usuario extends Doacao_Model_ModelAbstract {

    protected $u_id;
    protected $fb_id;
    protected $u_email;
    protected $u_senha;
    protected $u_dt_cad;
    protected $u_situacao;
    protected $u_tipo;
    protected $u_hash;

    public function __construct($usuario = null) {
        if (!is_null($usuario) && $usuario instanceof Zend_Db_Table_Row) {
            $this->u_id = $usuario->u_id;
            $this->fb_id = $usuario->fb_id;
            $this->u_email = $usuario->u_email;
            $this->u_senha = $usuario->u_senha;
            $this->u_dt_cad = $usuario->u_dt_cad;
            $this->u_situacao = $usuario->u_situacao;
            $this->u_tipo = $usuario->u_tipo;
            $this->u_hash = $usuario->u_hash;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
