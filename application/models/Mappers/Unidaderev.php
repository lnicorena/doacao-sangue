<?php

class Doacao_Model_Mapper_Unidaderev extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Unidaderev();
    }

    public function save(Doacao_Model_Unidaderev $unidaderev) {
        $dados = array(
            'rev_id' => $unidaderev->rev_id,
            'tun_id' => $unidaderev->tun_id,
            'un_id' => $unidaderev->un_id,
            'u_id' => $unidaderev->u_id,
            'rev_timestamp' => $unidaderev->rev_timestamp,
            'rev_nregistro' => $unidaderev->rev_nregistro,
            'rev_cnpj' => $unidaderev->rev_cnpj,
            'rev_nome' => $unidaderev->rev_nome,
            'rev_desc' => $unidaderev->rev_desc,
            'rev_uf' => $unidaderev->rev_uf,
            'rev_cidade' => $unidaderev->rev_cidade,
            'rev_cep' => $unidaderev->rev_cep,
            'rev_bairro' => $unidaderev->rev_bairro,
            'rev_rua' => $unidaderev->rev_rua,
            'rev_num' => $unidaderev->rev_num,
            'rev_obs' => $unidaderev->rev_obs,
            'rev_lat' => $unidaderev->rev_lat,
            'rev_lon' => $unidaderev->rev_lon,
        );

        if (is_null($unidaderev->rev_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('rev_id = ?' => $unidaderev->rev_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($unidaderev) {
        $this->_db_table->find($unidaderev->rev_id)->current()->delete();
    }

    public function getUnidaderevById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $unidaderev = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $unidaderev = new Doacao_Model_Unidaderev($tupla);
        }
        return $unidaderev;
    }

    public function getUnidaderevs() {
        return $this->_db_table->fetchAll(); // $select, "order by"
    }

}