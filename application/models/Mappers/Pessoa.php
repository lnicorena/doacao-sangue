<?php

class Doacao_Model_Mapper_Pessoa extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Pessoa();
    }

    public function save(Doacao_Model_Pessoa $pessoa) {
        $dados = array(
            'u_id' => $pessoa->u_id,
            'pes_nome' => $pessoa->pes_nome,
            'pes_apelido' => $pessoa->pes_apelido,
            'pes_rg' => $pessoa->pes_rg,
            'pes_cpf' => $pessoa->pes_cpf,
            'pes_sexo' => $pessoa->pes_sexo,
            'pes_uf' => $pessoa->pes_uf,
            'pes_cidade' => $pessoa->pes_cidade,
            'pes_lat' => $pessoa->pes_lat,
            'pes_lon' => $pessoa->pes_lon,
        );

        if ($this->_db_table->find($pessoa->u_id)->count() === 0) {
            //die(var_dump($dados));
            $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('u_id = ?' => $pessoa->u_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($pessoa) {
        $this->_db_table->find($pessoa->u_id)->current()->delete();
    }

    public function getPessoaById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $pessoa = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $pessoa = new Doacao_Model_Pessoa($tupla);
        }
        return $pessoa;
    }

    public function getPessoas() {
        return $this->_db_table->fetchAll(); // $select, "order by"
    }
    
}