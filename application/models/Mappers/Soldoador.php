<?php

class Doacao_Model_Mapper_Soldoador extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Soldoador();
    }

    public function save(Doacao_Model_Soldoador $soldoador) {
        $dados = array(
            'sd_id' => $soldoador->sd_id,
            'sol_id' => $soldoador->sol_id,
            'u_id' => $soldoador->u_id,
            'sd_timestamp' => $soldoador->sd_timestamp,
        );

        if (is_null($soldoador->sd_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('sd_id = ?' => $soldoador->sd_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($soldoador) {
        $this->_db_table->find($soldoador->sd_id)->current()->delete();
    }

    public function getSoldoadorById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $soldoador = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $soldoador = new Doacao_Model_Soldoador($tupla);
        }
        return $soldoador;
    }

    public function getSoldoadors($where=null, $count=null) {
        if($count)
            return $this->_db_table->fetchAll($where)->count();
        else
            return $this->_db_table->fetchAll($where);
    }
    
    public function getCountBySolic($id) {
        
    }

}