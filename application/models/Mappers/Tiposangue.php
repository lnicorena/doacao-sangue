<?php

class Doacao_Model_Mapper_Tiposangue extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Tiposangue();
    }

    public function save(Doacao_Model_Tiposangue $tiposangue) {
        $dados = array(
            'ts_cod' => $tiposangue->ts_cod,
            'ts_nome' => $tiposangue->ts_nome,
            'ts_desc' => $tiposangue->ts_desc,
        );

        if (is_null($tiposangue->ts_cod)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('ts_cod = ?' => $tiposangue->ts_cod));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($tiposangue) {
        $this->_db_table->find($tiposangue->ts_cod)->current()->delete();
    }

    public function getTiposangueById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $tiposangue = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $tiposangue = new Doacao_Model_Tiposangue($tupla);
        }
        return $tiposangue;
    }

    public function getTiposangues() {
        return $this->_db_table->fetchAll();
    }

}