<?php

class Doacao_Model_Mapper_Facebook extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Facebook();
    }

    public function save(Doacao_Model_Facebook $facebook) {
        $dados = array(
            'fb_id' => $facebook->fb_id,
            'fb_firstname' => $facebook->fb_firstname,
            'fb_name' => $facebook->fb_name,
            'fb_username' => $facebook->fb_username,
            'fb_sexo' => $facebook->fb_sexo,
            'fb_link' => $facebook->fb_link,
            'fb_email' => $facebook->fb_email,
            'fb_locale' => $facebook->fb_locale,
        );

        if ($this->_db_table->find($facebook->fb_id)->count() === 0) {
            $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('fb_id = ?' => $facebook->fb_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($facebook) {
        $this->_db_table->find($facebook->fb_id)->current()->delete();
    }

    public function getFacebookById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $facebook = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $facebook = new Doacao_Model_Facebook($tupla);
        }
        return $facebook;
    }


}