<?php

class Doacao_Model_Mapper_Tipounidade extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Tipounidade();
    }

    public function save(Doacao_Model_Tipounidade $tipounidade) {
        $dados = array(
            'tun_id' => $tipounidade->tun_id,
            'tun_nome' => $tipounidade->tun_nome,
            'tun_desc' => $tipounidade->tun_desc,
            'tun_param' => $tipounidade->tun_param,
        );

        if (is_null($tipounidade->tun_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('tun_id = ?' => $tipounidade->tun_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($tipounidade) {
        $this->_db_table->find($tipounidade->tun_id)->current()->delete();
    }

    public function getTipounidadeById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $tipounidade = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $tipounidade = new Doacao_Model_Tipounidade($tupla);
        }
        return $tipounidade;
    }

    public function getTiposUnidade() {
        return $this->_db_table->fetchAll(null,'tun_nome');
    }

}