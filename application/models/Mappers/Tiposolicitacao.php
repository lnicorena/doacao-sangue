<?php

class Doacao_Model_Mapper_Tiposolicitacao extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Tiposolicitacao();
    }

    public function save(Doacao_Model_Tiposolicitacao $tiposolicitacao) {
        $dados = array(
            'tsol_cod' => $tiposolicitacao->tsol_cod,
            'tsol_nome' => $tiposolicitacao->tsol_nome,
        );

        if (is_null($tiposolicitacao->tsol_cod)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('tsol_cod = ?' => $tiposolicitacao->tsol_cod));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($tiposolicitacao) {
        $this->_db_table->find($tiposolicitacao->tsol_cod)->current()->delete();
    }

    public function getTiposolicitacaoById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $tiposolicitacao = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $tiposolicitacao = new Doacao_Model_Tiposolicitacao($tupla);
        }
        return $tiposolicitacao;
    }

    public function getTiposolicitacao() {
        return $this->_db_table->fetchAll(null,'tsol_nome');
    }

}