<?php

class Doacao_Model_Mapper_Tipoalerta extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Tipoalerta();
    }

    public function save(Doacao_Model_Tipoalerta $tipoalerta) {
        $dados = array(
            'tal_id' => $tipoalerta->tal_id,
            'tal_nome' => $tipoalerta->tal_nome,
            'tal_desc' => $tipoalerta->tal_desc,
        );

        if (is_null($tipoalerta->tal_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('tal_id = ?' => $tipoalerta->tal_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($tipoalerta) {
        $this->_db_table->find($tipoalerta->tal_id)->current()->delete();
    }

    public function getTipoalertaById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $tipoalerta = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $tipoalerta = new Doacao_Model_Tipoalerta($tupla);
        }
        return $tipoalerta;
    }

    public function getTipoalertas() {
        return $this->_db_table->fetchAll(null,'tal_nome');
    }

}