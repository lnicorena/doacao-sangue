<?php

class Doacao_Model_Mapper_Tipodenuncia extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Tipodenuncia();
    }

    public function save(Doacao_Model_Tipodenuncia $tipodenuncia) {
        $dados = array(
            'tden_cod' => $tipodenuncia->tden_cod,
            'tden_nome' => $tipodenuncia->tden_nome,
            'tden_desc' => $tipodenuncia->tden_desc,
        );

        if (is_null($tipodenuncia->tden_cod)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('tden_cod = ?' => $tipodenuncia->tden_cod));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($tipodenuncia) {
        $this->_db_table->find($tipodenuncia->tden_cod)->current()->delete();
    }

    public function getTipodenunciaById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $tipodenuncia = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $tipodenuncia = new Doacao_Model_Tipodenuncia($tupla);
        }
        return $tipodenuncia;
    }

    public function getTiposDenuncia() {
        return $this->_db_table->fetchAll();
    }

}