<?php

class Doacao_Model_Mapper_Denuncia extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Denuncia();
    }

    public function save(Doacao_Model_Denuncia $denuncia) {
        $dados = array(
		            'den_id' => $denuncia->den_id,
		            'un_rev_id' => $denuncia->un_rev_id,
		            'tden_cod' => $denuncia->tden_cod,
		            'u_id_denunciado' => $denuncia->u_id_denunciado,
		            'u_id' => $denuncia->u_id,
		            'den_timestamp' => $denuncia->den_timestamp,
		            'den_situacao' => $denuncia->den_situacao,
		            'den_text' => $denuncia->den_text,
		        );

        if (is_null($denuncia->den_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('den_id = ?' => $denuncia->den_id));
        }
    }
	
    public function getDbTable(){
        return $this->_db_table;
    }
    
    public function remove($denuncia) {
        $this->_db_table->find($denuncia->den_id)->current()->delete();
    }
	
    public function getDenunciaById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $denuncia = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $denuncia = new Doacao_Model_Denuncia($tupla);
        }
        return $denuncia;
    }
    
    public function getDenuncias() {
        return $this->_db_table->fetchAll(); // $select, "order by"
    }

}