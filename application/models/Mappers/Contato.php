<?php

class Doacao_Model_Mapper_Contato extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Contato();
    }

    public function save(Doacao_Model_Contato $contato) {
        $dados = array(
            'con_id' => $contato->con_id,
            'con_nome' => $contato->con_nome,
            'con_email' => $contato->con_email,
            'con_assunto' => $contato->con_assunto,
            'con_msg' => $contato->con_msg,
            'con_ip' => $contato->con_ip,
            'con_timestamp' => $contato->con_timestamp,
            'con_uid' => $contato->con_uid,
            'con_status' => $contato->con_status,
        );

        if (is_null($contato->con_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('con_id = ?' => $contato->con_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($contato) {
        $this->_db_table->find($contato->con_id)->current()->delete();
    }

    public function getMensagemById($id, $vetor = false, $usuario=false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }
        if($usuario){
            if($vetor)
                return $busca->current()->findParentRow('Doacao_Model_DbTable_Usuario')->toArray();
            else
                return $busca->current()->findParentRow('Doacao_Model_DbTable_Usuario');
        }

        if ($vetor) {
            $contato = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $contato = new Doacao_Model_Contato($tupla);
        }
        return $contato;
    }
    
    public function getMensagens($where=null) {
        return $this->_db_table->fetchAll($where,'con_timestamp DESC');
    }

}