<?php

class Doacao_Model_Mapper_Unidade extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Unidade();
    }

    public function save(Doacao_Model_Unidade $unidade) {
        $dados = array(
            'un_id' => $unidade->un_id,
            'un_situacao' => $unidade->un_situacao,
            'un_modificacoes' => $unidade->un_modificacoes,
        );

        if (is_null($unidade->un_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('un_id = ?' => $unidade->un_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($unidade) {
        $this->_db_table->find($unidade->un_id)->current()->delete();
    }

    public function getUnidadeById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $unidade = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $unidade = new Doacao_Model_Unidade($tupla);
        }
        return $unidade;
    }

    public function getUnidades($where = null, $order = null, $limit=null, $debug = false) {
        $select = $this->_db_table->select()
                ->setIntegrityCheck(false)
                ->from(array('un' => 'v_unidades'))
                ->joinInner(array('u' => 'usuario'), 'u.u_id = un.u_id', null)
                ->joinInner(array('p' => 'pessoa'), 'u.u_id = p.u_id', 'pes_nome')
                ->joinInner(array('tun' => 'tipounidade'), 'tun.tun_id = un.tun_id', array('tun_nome', 'tun_desc'));

        if($limit)
            $select->limit($limit);
        else
            $select->limit("10000");

        if ($order) {
            $select->order($order);
        } else {
            $select->order('RANDOM()');
            $select->columns('setseed(' . date('d') / 100 . ')');
        }

        if (isset($where)) {
            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    $select->where($key, $value);
                }
            } elseif ($where != "") {
                $select->where($where);
            }
        } else {
            if (isset(Zend_Registry::get('usuario')->u_id)) {
                $umapper = new Doacao_Model_Mapper_Usuario();
                $u = $umapper->getUsuarios("u.u_id = " . (int) Zend_Registry::get('usuario')->u_id)->current();
                if ((isset($u->pes_cidade) && $u->pes_cidade != "") || (isset($u->pes_uf) && $u->pes_uf != "")) {
                    
                    if ($u->pes_cidade != "") {
                        $select->orWhere("un.rev_cidade ILIKE '%' || ? || '%'", $u->pes_cidade);
                    }
                    
                    if ($u->pes_uf != "") {
                        $select->orWhere("un.rev_uf ILIKE '%' || ? || '%'", $u->pes_uf);
                    }
                    
                    $select->reset(Zend_Db_Select::ORDER);
                    $select->reset(Zend_Db_Select::LIMIT_COUNT);
                    $sql = $select->__toString();
                    $select->reset(Zend_Db_Select::WHERE);
                    
                    if ($u->pes_cidade != "") {
                        $select->where("un.rev_cidade NOT ILIKE '%' || ? || '%'", $u->pes_cidade);
                    }
                    if ($u->pes_uf != "") {
                        $select->where("un.rev_uf NOT ILIKE '%' || ? || '%'", $u->pes_uf);
                    }
                    
                    
                    $sql .= " union all " . $select->__toString() . " limit 10000";
                }
            }
        }

        if (isset($sql) && $sql != "") {
            if ($debug)
                exit($sql);
            
            return $this->_db_table->getDefaultAdapter()->query($sql)->fetchAll(PDO::FETCH_CLASS);
        } else {
            if ($debug)
                exit($select->__toString());
            
            return $this->_db_table->fetchAll($select);
        }
        
    }

}