<?php

class Doacao_Model_Mapper_Log extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Log();
    }

    public function save(Doacao_Model_Log $log) {
        $dados = array(
            'log_id' => $log->log_id,
            'u_id' => $log->u_id,
            'log_nivel' => $log->log_nivel,
            'log_timestamp' => $log->log_timestamp,
            'log_text' => $log->log_text,
        );

        if (is_null($log->log_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('log_id = ?' => $log->log_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($log) {
        $this->_db_table->find($log->log_id)->current()->delete();
    }

    public function getLogById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $log = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $log = new Doacao_Model_Log($tupla);
        }
        return $log;
    }

    public function getLogs($nivel=null, $debug=false) {
        $select = $this->_db_table->select()
                ->setIntegrityCheck(false)
                ->from(array('l' => 'log'))
                ->joinLeft(array('u' => 'usuario'), 'u.u_id = l.u_id ')
                ->limit(10000)
                ->order('log_timestamp DESC');
        
        if($nivel) $select->where('log_nivel = ?',$nivel);
        
        if($debug) exit($select->__toString());
        
        return $this->_db_table->fetchAll($select);
    }

}