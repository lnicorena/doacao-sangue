<?php

class Doacao_Model_Mapper_Solicitacao extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Solicitacao();
    }

    public function save(Doacao_Model_Solicitacao $solicitacao) {
        $dados = array(
            'sol_id' => $solicitacao->sol_id,
            'ts_cod' => $solicitacao->ts_cod,
            'tsol_cod' => $solicitacao->tsol_cod,
            'un_id' => $solicitacao->un_id,
            'u_id' => $solicitacao->u_id,
            'sol_dt' => $solicitacao->sol_dt,
            'sol_qtd' => $solicitacao->sol_qtd,
            'sol_paciente' => $solicitacao->sol_paciente,
            'sol_situacao' => $solicitacao->sol_situacao,
            'sol_detalhes' => $solicitacao->sol_detalhes,
        );

        if (is_null($solicitacao->sol_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('sol_id = ?' => $solicitacao->sol_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }

    public function remove($solicitacao) {
        $this->_db_table->find($solicitacao->sol_id)->current()->delete();
    }

    public function getSolicitacaoById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $solicitacao = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $solicitacao = new Doacao_Model_Solicitacao($tupla);
        }
        return $solicitacao;
    }

    public function getSolicitacoes($where, $limit=null, $debug = false) {
        $select = $this->_db_table->select()
                ->setIntegrityCheck(false)
                ->from(array('sol' => 'solicitacao'))
                ->joinInner(array('tsol' => 'tiposolicitacao'), 'tsol.tsol_cod = sol.tsol_cod')
                ->joinInner(array('ts' => 'tiposangue'), 'ts.ts_cod = sol.ts_cod')
                ->joinInner(array('un' => 'v_unidades'), 'un.un_id = sol.un_id')
                ->joinInner(array('u' => 'usuario'), 'u.u_id = sol.u_id')
                ->joinInner(array('p' => 'pessoa'), 'u.u_id = p.u_id')
                ->joinInner(array('tun' => 'tipounidade'), 'tun.tun_id = un.tun_id', array('tun_nome', 'tun_desc'))
                ->order('sol_dt DESC');

        if (isset($where)) {
            if (is_array($where) && count($where) > 0) {
                foreach ($where as $key => $value) {
                    $select->where($key, $value);
                }
            } elseif (is_string ($where) && $where != "") {
                $select->where($where);
            }
        }
        
        if($limit)
            $select->limit ($limit);
        
        if ($debug)
            exit($select->__toString());

        return $this->_db_table->fetchAll($select);
    }

}