<?php

class Doacao_Model_Mapper_Usuario extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Usuario();
    }

    public function save(Doacao_Model_Usuario $usuario) {
        $dados = array(
            'u_id' => $usuario->u_id,
            'fb_id' => $usuario->fb_id,
            'u_email' => $usuario->u_email,
            'u_senha' => $usuario->u_senha,
            'u_dt_cad' => $usuario->u_dt_cad,
            'u_situacao' => $usuario->u_situacao,
            'u_tipo' => $usuario->u_tipo,
            'u_hash' => $usuario->u_hash,
        );

        if (is_null($usuario->u_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('u_id = ?' => $usuario->u_id));
        }
    }

    public function getDbTable() {
        return $this->_db_table;
    }
    
    public function remove($usuario) {
        $this->_db_table->find($usuario->u_id)->current()->delete();
    }

    public function getUsuarioById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $usuario = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $usuario = new Doacao_Model_Usuario($tupla);
        }
        return $usuario;
    }

    public function getUsuarios($where=null, $debug=false) {
        $select = $this->_db_table->select()
                ->setIntegrityCheck(false)
                ->from(array('u' => 'usuario'))
                ->joinInner(array('p' => 'pessoa'), 'p.u_id = u.u_id ')
                ->joinLeft(array('f' => 'facebook'), 'f.fb_id = u.fb_id ');
        
        if($where) $select->where($where);
        
        if($debug) exit($select->__toString());
        
        return $this->_db_table->fetchAll($select);
    }

    
    public function emailExists($email) {
        return $this->_db_table->fetchAll($this->_db_table->select()->where("u_email = ?", $email))->count() > 0;
    }
    
    public function getUserByEmail($email) {
        $result = $this->_db_table->fetchAll($this->_db_table->select()->where("u_email = ?", $email));
        if($result->count() === 0)
            return false;
        else
            return new Doacao_Model_Usuario($result->current());
    }
    
    public function getUserByFId($fb_id) {
        $result = $this->_db_table->fetchAll($this->_db_table->select()->where("fb_id = ?", $fb_id));
        if($result->count() === 0)
            return false;
        else
            return new Doacao_Model_Usuario($result->current());
    }
    
    public function validaSenhaAtual($id, $senha, $debug = false) {
        $select = $this->_db_table->select()
                ->from("usuario", "COUNT(u_id) AS qtd")
                ->where("u_id = ?", (int) $id)
                ->where("u_senha = md5(?)", $senha);

        $v = $this->_db_table->fetchRow($select)->qtd;

        if ($debug)
            die($select->__toString());

        if ($v == 1)
            return true;
        else
            return false;
    }

    public function login($usuario, $senha, $lembrar = 0, $fblogin=0) {

        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('EuDoador'));
        $authAdapter = new Zend_Auth_Adapter_DbTable($this->_db_table->getDefaultAdapter());

        $authAdapter->setTableName(array('u' => 'usuario'))
                ->setIdentityColumn('u.u_email')
                ->setIdentity($usuario);
        if($fblogin){
            $authAdapter->setCredentialColumn('u.fb_id')
                        ->setCredential($senha);
        }else{
            $authAdapter->setCredentialColumn('u.u_senha')
                        ->setCredential(md5($senha));
        }
        $join = $authAdapter->getDbSelect();
        $join->joinInner(array('p' => 'pessoa'), 'p.u_id = u.u_id');

        $result = $authAdapter->authenticate();
        //var_dump($usuario);var_dump($senha);var_dump($fblogin);var_dump($result->isValid());die;
        if ($result->isValid()) {
            $auth->getStorage()->write($authAdapter->getResultRowObject(
                            array('u_id', 'pes_nome', 'pes_apelido', 'u_email', 'u_tipo', 'u_situacao','fb_id')));
            if ($auth->getIdentity()->u_situacao == -1) {
                $auth->clearIdentity();
                Zend_Session::forgetMe();
                return -1;
            }
            $session = new Zend_Session_Namespace('Zend_Auth');
            $session->setExpirationSeconds(24 * 3600);
            if ($lembrar == 1) {
                Zend_Session::rememberMe();
            }
            return true;
        }
        return false;
    }

}