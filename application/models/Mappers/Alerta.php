<?php

class Doacao_Model_Mapper_Alerta extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Alerta();
    }

    public function save(Doacao_Model_Alerta $alerta) {
        $dados = array(
		            'al_cod' => $alerta->al_cod,
		            'tal_id' => $alerta->tal_id,
		            'u_id' => $alerta->u_id,
		        );

        if (is_null($alerta->al_cod)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('al_cod = ?' => $alerta->al_cod));
        }
    }
	
    public function getDbTable(){
        return $this->_db_table;
    }
    
    public function remove($alerta) {
        $this->_db_table->find($alerta->al_cod)->current()->delete();
    }
	
    public function getAlertaById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $alerta = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $alerta = new Doacao_Model_Alerta($tupla);
        }
        return $alerta;
    }
    
    public function getAlertas() {
        return $this->_db_table->fetchAll(); // $select, "order by"
    }

}