<?php

class Doacao_Model_Mapper_Solshare extends Doacao_Model_Mapper_MapperAbstract {

    protected $_db_table;

    public function __construct() {
        $this->_db_table = new Doacao_Model_DbTable_Solshare();
    }

    public function save(Doacao_Model_Solshare $solshare) {
        $dados = array(
		            'share_id' => $solshare->share_id,
		            'sol_id' => $solshare->sol_id,
		            'u_id' => $solshare->u_id,
		            'share_dt' => $solshare->share_dt,
		        );

        if (is_null($solshare->share_id)) {
            return $this->_db_table->insert($dados);
        } else {
            $this->_db_table->update($dados, array('share_id = ?' => $solshare->share_id));
        }
    }
	
    public function getDbTable(){
        return $this->_db_table;
    }
    
    public function remove($solshare) {
        $this->_db_table->find($solshare->share_id)->current()->delete();
    }
	
    public function getSolshareById($id, $vetor = false) {
        $busca = $this->_db_table->find($id);

        if (count($busca) == 0) {
            throw new Exception('Informação não encontrada');
        }

        if ($vetor) {
            $solshare = $busca->current()->toArray();
        } else {
            $tupla = $busca->current();
            $solshare = new Doacao_Model_Solshare($tupla);
        }
        return $solshare;
    }
    
    public function getSolshares() {
        return $this->_db_table->fetchAll(); // $select, "order by"
    }

}