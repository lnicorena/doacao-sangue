<?php

class Doacao_Model_DbTable_Solshare  extends Doacao_Model_DbTable_TableAbstract {
    
    protected $_name = 'solshare';

    protected $_id = 'share_id';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'FkSolshareShareSolSolicita' => array(
          	'columns' => 'sol_id',
            'refTableClass' => 'Doacao_Model_DbTable_Solicitacao',
            'refColumns' => 'sol_id'
        ),
        'FkSolshareShareUsuUsuario' => array(
          	'columns' => 'u_id',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );
    	
}
