<?php

class Doacao_Model_DbTable_Denuncia  extends Doacao_Model_DbTable_TableAbstract {
    
    protected $_name = 'denuncia';

    protected $_id = 'den_id';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'FkDenunciaDenRevUUnidader' => array(
          	'columns' => 'un_rev_id',
            'refTableClass' => 'Doacao_Model_DbTable_Unidaderev',
            'refColumns' => 'rev_id'
        ),
        'FkDenunciaDenTdenuTipodenu' => array(
          	'columns' => 'tden_cod',
            'refTableClass' => 'Doacao_Model_DbTable_Tipodenuncia',
            'refColumns' => 'tden_cod'
        ),
        'FkDenunciaDenUacusUsuario' => array(
          	'columns' => 'u_id_denunciado',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        ),
        'FkDenunciaDenUdenuUsuario' => array(
          	'columns' => 'u_id',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );
    	
}
