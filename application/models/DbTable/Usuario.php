<?php

class Doacao_Model_DbTable_Usuario extends Doacao_Model_DbTable_TableAbstract {

    protected $_name = 'usuario';
    
    protected $_id = 'u_id';
    
    protected $_sequence = true;
    
    protected $_referenceMap = array(
        'FkUsuarioUsuFbFacebook' => array(
            'columns' => 'fb_id',
            'refTableClass' => 'Doacao_Model_DbTable_Facebook',
            'refColumns' => 'fb_id'
        )
    );

}
