<?php

class Doacao_Model_DbTable_Facebook  extends Doacao_Model_DbTable_TableAbstract {
    
    protected $_name = 'facebook';

    protected $_id = 'fb_id';

    protected $_sequence = true;
    	
}
