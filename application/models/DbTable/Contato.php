<?php

class Doacao_Model_DbTable_Contato extends Doacao_Model_DbTable_TableAbstract {

    protected $_name = 'contato';
    
    protected $_id = 'con_id';
    
    protected $_sequence = true;
    
    protected $_referenceMap = array(
        'FK_Contato_Usuario' => array(
            'columns' => 'con_uid',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );

}
