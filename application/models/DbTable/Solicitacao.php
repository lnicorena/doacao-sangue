<?php

class Doacao_Model_DbTable_Solicitacao extends Doacao_Model_DbTable_TableAbstract {

    protected $_name = 'solicitacao';
    
    protected $_id = 'sol_id';
    
    protected $_sequence = true;
    
    protected $_referenceMap = array(
        'FkSolicitaSolicTsaTtiposan' => array(
            'columns' => 'ts_cod',
            'refTableClass' => 'Doacao_Model_DbTable_Ttiposangue',
            'refColumns' => 'ts_cod'
        ),
        'FkSolicitaSolicTsoTiposoli' => array(
            'columns' => 'tsol_cod',
            'refTableClass' => 'Doacao_Model_DbTable_Tiposolicitacao',
            'refColumns' => 'tsol_cod'
        ),
        'FkSolicitaUnSolicUnidade' => array(
            'columns' => 'un_id',
            'refTableClass' => 'Doacao_Model_DbTable_Unidade',
            'refColumns' => 'un_id'
        ),
        'FkSolicitaUsuSolicUsuario' => array(
            'columns' => 'u_id',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );

}
