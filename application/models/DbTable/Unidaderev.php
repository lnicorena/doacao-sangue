<?php

class Doacao_Model_DbTable_Unidaderev  extends Doacao_Model_DbTable_TableAbstract {
    
    protected $_name = 'unidaderev';

    protected $_id = 'rev_id';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'FkUnidaderUnRevTuTipounid' => array(
          	'columns' => 'tun_id',
            'refTableClass' => 'Doacao_Model_DbTable_Tipounidade',
            'refColumns' => 'tun_id'
        ),
        'FkUnidaderUnRevUnUnidade' => array(
          	'columns' => 'un_id',
            'refTableClass' => 'Doacao_Model_DbTable_Unidade',
            'refColumns' => 'un_id'
        ),
        'FkUnidaderUnRevUsUsuario' => array(
          	'columns' => 'u_id',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );
    	
}
