<?php

class Doacao_Model_DbTable_Pessoa  extends Doacao_Model_DbTable_TableAbstract {
    
    protected $_name = 'pessoa';

    protected $_id = 'u_id';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'FkPessoaUsuPesUsuario' => array(
          	'columns' => 'u_id',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );
    	
}
