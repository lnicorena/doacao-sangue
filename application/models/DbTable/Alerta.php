<?php

class Doacao_Model_DbTable_Alerta  extends Doacao_Model_DbTable_TableAbstract {
    
    protected $_name = 'alerta';

    protected $_id = 'al_cod';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'FkAlertaAlertaTaTipoaler' => array(
          	'columns' => 'tal_id',
            'refTableClass' => 'Doacao_Model_DbTable_Tipoalerta',
            'refColumns' => 'tal_id'
        ),
        'FkAlertaAlertaUsUsuario' => array(
          	'columns' => 'u_id',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );
    	
}
