<?php

class Doacao_Model_DbTable_Log  extends Doacao_Model_DbTable_TableAbstract {
    
    protected $_name = 'log';

    protected $_id = 'log_id';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'FkLogUsuLogUsuario' => array(
            'columns' => 'u_id',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );
    	
}
