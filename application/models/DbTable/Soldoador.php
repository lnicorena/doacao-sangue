<?php

class Doacao_Model_DbTable_Soldoador  extends Doacao_Model_DbTable_TableAbstract {
    
    protected $_name = 'soldoador';

    protected $_id = 'sd_id';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'FkSoldoadoSolicdoadSolicita' => array(
          	'columns' => 'sol_id',
            'refTableClass' => 'Doacao_Model_DbTable_Solicitacao',
            'refColumns' => 'sol_id'
        ),
        'FkSoldoadoSolicdoadUsuario' => array(
          	'columns' => 'u_id',
            'refTableClass' => 'Doacao_Model_DbTable_Usuario',
            'refColumns' => 'u_id'
        )
    );
    	
}
