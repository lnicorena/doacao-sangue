<?php

class Doacao_Model_Tipodenuncia extends Doacao_Model_ModelAbstract {

    protected $tden_cod;
    protected $tden_nome;
    protected $tden_desc;

    public function __construct($tipodenuncia = null) {
        if (!is_null($tipodenuncia) && $tipodenuncia instanceof Zend_Db_Table_Row) {
            $this->tden_cod = $tipodenuncia->tden_cod;
            $this->tden_nome = $tipodenuncia->tden_nome;
            $this->tden_desc = $tipodenuncia->tden_desc;
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
