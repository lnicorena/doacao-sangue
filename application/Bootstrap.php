<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initAutoload() {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(true);

        $defaultLoader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Doacao',
            'basePath' => APPLICATION_PATH
        ));

        $defaultLoader->addResourceType('Mapper', 'models/Mappers', 'Model_Mapper');

//        $doacaoLoader = new Zend_Application_Module_Autoloader(array(
//            'namespace' => 'Doacao',
//            'basePath' => APPLICATION_PATH . '/modules/doacao'
//        ));
    }

    protected function _initConfiguration() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('HTML5');
        $view->headTitle('Eu Doador :: Doe sangue você também');

        $application = $this->getOption('applicattion');
        Zend_Registry::set('applicattion', $application ['name']);

        $facebook = $this->getOption('facebook');
        Zend_Registry::set('fb', json_decode(json_encode($facebook), FALSE)); // PHP 5.3- não suporta acesso direto a arrays
        
        $format = new Utils_Plugin_Format ();
        Zend_Registry::set('format', $format);

        $notifica = new Utils_Plugin_Notifica($application ['email']);
        Zend_Registry::set('notifica', $notifica);

        $logger = new Utils_Log();
        Zend_Registry::set('logger', $logger);
        
        $bitly = new Utils_Bitly($application ['bitly']);
        Zend_Registry::set('bitly', $bitly);

        Zend_Registry::set('itens_por_pagina', 50);
        Zend_Registry::set('range', 7);
        
        Zend_Paginator::setDefaultScrollingStyle('Sliding');
        Zend_View_Helper_PaginationControl::setDefaultViewPartial( 'paginacao.phtml' );
    }

    protected function _initFeedbacks() {
        $flashMsgHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
        $messages = $flashMsgHelper->getMessages();

        Zend_Registry::set('feedback', $messages);
    }

    protected function _initAuth() {
        $this->bootstrap('FrontController');
        $this->_frontController = $this->getResource('FrontController');
        $routeador = $this->_frontController->getRouter();
        $requisicao = new Zend_Controller_Request_Http ();
        $routeador->route($requisicao);
        $modulo = $requisicao->getModuleName();
        $controller = $requisicao->getControllerName();
        $action = $requisicao->getActionName();

        $login = $requisicao->getBaseUrl() . '/usuario';

        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('EuDoador'));

        Zend_Registry::set('usuario', $auth->getIdentity());

        $frontController = Zend_Controller_Front::getInstance();
        $frontController->registerPlugin(new Utils_Plugin_ACL());
        
    }

    protected function _initNavigation() {
        Zend_View_Helper_PaginationControl::setDefaultViewPartial(array(
            'navegation/paginacao.phtml',
            'default'
        ));
    }

    protected function initCompress() {
        if (APPLICATION_ENV == 'production') {
            $front = Zend_Controller_Front::getInstance();
            $front->registerPlugin(new Utils_Plugin_Compress(), 150);
        }
    }

}