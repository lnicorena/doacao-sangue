<?php

class Doacao_Form_Denuncia extends Utils_Form_Decorator_Default {

    private $ud_id;

    public function __construct($uid = 0) {
        $this->ud_id = $uid;
        parent::__construct();
        self::init();
    }

    public function init() {

        $this->setName('form-denuncia');
        $this->setAttrib('class', 'zend_form form-horizontal');
        $this->setMethod('POST');

        $id = new Zend_Form_Element_Hidden('den_id');
        $id->addFilter('Int');
        $this->addElement($id);
        
        $udid = new Zend_Form_Element_Hidden('u_id_denunciado');
        $udid->addFilter('Int')
                ->setValue($this->ud_id);
        $this->addElement($udid);

        $tden = new Doacao_Model_Mapper_Tipodenuncia();
        $TdenCod = new Zend_Form_Element_Select('tden_cod');
        $TdenCod->setLabel('Tipo de denúncia:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $TdenCod->addMultiOptions(array('' => ' Selecione'));
        foreach ($tden->getTiposDenuncia() as $t)
            $TdenCod->addMultiOptions(array($t->tden_cod => $t->tden_nome));
        $this->addElement($TdenCod);

        $DenText = new Zend_Form_Element_Textarea('den_text');
        $DenText->setLabel('Informações adicionais:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->setAttrib('rows', '5');
        $this->addElement($DenText);


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Confirmar')
                ->setAttrib('class', 'btn btn-large btn-danger')
                ->setIgnore(true);
        $this->addElement($submit);

        $limpar = new Zend_Form_Element_Button('limpar');
        $limpar->setLabel('Voltar')
                ->setAttrib('onclick', 'window.history.go(-1)')
                ->setAttrib('class', 'btn btn-large')
                ->setIgnore(true);
        $this->addElement($limpar);

        $this->configurandoTamanho('tden_cod', 'span8');
        $this->configurandoTamanho('den_text', 'span12');

        $this->montandoGrupo(array('tden_cod'), 'tipoden');
        $this->montandoGrupo(array('den_text'), 'conteudo');


        $this->montandoGrupo(array('submit', 'limpar'), 'botoes');
    }

}