<?php

class Doacao_Form_Alerta extends Utils_Form_Decorator_Default {

    public function init() {

    $this->setName('form-alerta');
        $this->setAttrib('class', 'zend_form form-horizontal');
        $this->setMethod('POST');

        $id = new Zend_Form_Element_Hidden('al_cod');
        $id->addFilter('Int');
        $this->addElement($id);

                $TalId = new Zend_Form_Element_Text('tal_id');
        $TalId->setLabel('TalId:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($TalId);
        $this->configurandoTamanho('tal_id', 'span3');
        
                $UId = new Zend_Form_Element_Text('u_id');
        $UId->setLabel('UId:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($UId);
        $this->configurandoTamanho('u_id', 'span3');
        
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Salvar')
                ->setAttrib('class', 'btn btn-large btn-primary')
                ->setIgnore(true);
        $this->addElement($submit);

        $limpar = new Zend_Form_Element_Button('limpar');
        $limpar->setLabel('Limpar')
                ->setAttrib('type', 'reset')
                ->setAttrib('class', 'btn btn-large btn-warning')
                ->setIgnore(true);
        $this->addElement($limpar);

        $this->montandoGrupo(array('submit', 'limpar'), 'botoes');
    }

}