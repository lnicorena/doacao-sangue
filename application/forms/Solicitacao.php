<?php

class Doacao_Form_Solicitacao extends Utils_Form_Decorator_Default {

    public function init() {

        $this->setName('form-solicitacao');
        $this->setAttrib('class', 'zend_form form-horizontal');
        $this->setMethod('POST');

        $id = new Zend_Form_Element_Hidden('sol_id');
        $id->addFilter('Int');
        $this->addElement($id);

        $tsmapper = new Doacao_Model_Mapper_Tiposangue();
        $TsCod = new Zend_Form_Element_Select('ts_cod');
        $TsCod->setLabel('Tipo sanguíneo:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addMultiOptions(array('' => ' Selecione'));
        foreach ($tsmapper->getTiposangues() as $ts)
            $TsCod->addMultiOptions(array($ts->ts_cod => $ts->ts_nome));
        $this->addElement($TsCod);

        $tsolmapper = new Doacao_Model_Mapper_Tiposolicitacao();
        $TsolCod = new Zend_Form_Element_Select('tsol_cod');
        $TsolCod->setLabel('Solicitação:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addMultiOptions(array('' => ' Selecione'));
        foreach ($tsolmapper->getTiposolicitacao() as $ts)
            $TsolCod->addMultiOptions(array($ts->tsol_cod => $ts->tsol_nome));
        $this->addElement($TsolCod);


        $unmapper = new Doacao_Model_Mapper_Unidade();
        $UnId = new Zend_Form_Element_Select('un_id');
        $UnId->setLabel('Unidade:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->setAttrib('placeholder', 'Selecione a unidade')
                ->addMultiOptions(array('' => ''));
        foreach ($unmapper->getUnidades() as $u)
            $UnId->addMultiOptions(array($u->un_id => "{$u->rev_nome} ({$u->rev_cidade} - {$u->rev_uf})"));
        $this->addElement($UnId);


        $SolQtd = new Zend_Form_Element_Text('sol_qtd');
        $SolQtd->setLabel('Quantidade de bolsas:')
                ->addFilter('StripTags')
                ->addFilter('StringTrim');
        $this->addElement($SolQtd);


        $SolPaciente = new Zend_Form_Element_Text('sol_paciente');
        $SolPaciente->setLabel('Nome do paciente:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($SolPaciente);


        $SolDet = new Zend_Form_Element_Textarea('sol_detalhes');
        $SolDet->setLabel('Informações adicionais:')
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->setAttrib('rows', '5');
        $this->addElement($SolDet);

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Concluir solicitação')
                ->setAttrib('class', 'btn btn-large btn-primary')
                ->setIgnore(true);
        $this->addElement($submit);

        $limpar = new Zend_Form_Element_Button('limpar');
        $limpar->setLabel('Limpar')
                ->setAttrib('type', 'reset')
                ->setAttrib('class', 'btn btn-large btn-warning')
                ->setIgnore(true);
        $this->addElement($limpar);


        $this->configurandoTamanho('un_id', 'span12');
        $this->configurandoTamanho('tsol_cod', 'span5');
        $this->configurandoTamanho('ts_cod', 'span4');
        $this->configurandoTamanho('sol_qtd', 'span3');
        $this->configurandoTamanho('sol_paciente', 'span4');
        $this->configurandoTamanho('sol_detalhes', 'span8');
        
        
        $this->montandoGrupo(array('un_id'), 'unid');
        $this->montandoGrupo(array('tsol_cod','ts_cod','sol_qtd'), 'infos');
        $this->montandoGrupo(array('sol_paciente','sol_detalhes'), 'detalhes');
        $this->montandoGrupo(array('submit', 'limpar'), 'botoes');
    }

}