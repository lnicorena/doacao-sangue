<?php

class Doacao_Form_Usuario extends Utils_Form_Decorator_Default {

    public function init() {

        $this->setName('form-usuario');
        $this->setAttrib('class', 'zend_form form-horizontal');
        $this->setMethod('POST');

        $id = new Zend_Form_Element_Hidden('u_id');
        $id->addFilter('Int');
        $this->addElement($id);

        $PesNome = new Zend_Form_Element_Text('pes_nome');
        $PesNome->setLabel('Nome Completo:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($PesNome);
        
        $PesApelido = new Zend_Form_Element_Text('pes_apelido');
        $PesApelido->setLabel('Apelido:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($PesApelido);

        $PesRg = new Zend_Form_Element_Text('pes_rg');
        $PesRg->setLabel('RG:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($PesRg);

        $PesCpf = new Zend_Form_Element_Text('pes_cpf');
        $PesCpf->setLabel('CPF:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($PesCpf);

        $PesSexo = new Zend_Form_Element_Select('pes_sexo');
        $PesSexo->setLabel('Sexo:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addMultiOptions(array('' => ' Selecione','M' => ' Masculino', 'F' => ' Feminino'));
        $this->addElement($PesSexo);
        
        $UEmail = new Zend_Form_Element_Text('u_email');
        $UEmail->setLabel('Email:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($UEmail);

        $USenha = new Zend_Form_Element_Password('u_senha');
        $USenha->setLabel('Senha:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($USenha);
        
        $USenha2 = new Zend_Form_Element_Password('u_senha2');
        $USenha2->setLabel('Confirme a senha:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($USenha2);
        
        $UF = new Zend_Form_Element_Select('pes_uf');
        $UF->setLabel('Estado:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addMultiOptions(array('' => ' Selecione', 'RS' => ' Rio Grande do Sul'));
        $this->addElement($UF);
        
        $cidade = new Zend_Form_Element_Select('pes_cidade');
        $cidade->setLabel('Cidade:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addMultiOptions(array('' => ' Selecione', 'SM' => ' Santa Maria'));
        $this->addElement($cidade);
        
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Salvar')
                ->setAttrib('class', 'btn btn-large btn-primary')
                ->setIgnore(true);
        $this->addElement($submit);

        $limpar = new Zend_Form_Element_Button('limpar');
        $limpar->setLabel('Limpar')
                ->setAttrib('type', 'reset')
                ->setAttrib('class', 'btn btn-large btn-warning')
                ->setIgnore(true);
        $this->addElement($limpar);

        
        $this->configurandoTamanho('pes_nome', 'span6');
        $this->configurandoTamanho('pes_apelido', 'span4');
        $this->configurandoTamanho('pes_rg', 'span3');
        $this->configurandoTamanho('pes_cpf', 'span3');
        $this->configurandoTamanho('pes_sexo', 'span2');
        $this->configurandoTamanho('u_email', 'span6');
        $this->configurandoTamanho('pes_uf', 'span4');
        $this->configurandoTamanho('pes_cidade', 'span6');
        $this->configurandoTamanho('u_senha', 'span3');
        $this->configurandoTamanho('u_senha2', 'span3');
        
        $this->montandoGrupo(array('pes_nome'), 'grupo1');
        $this->montandoGrupo(array('pes_apelido'), 'grupo2');
        $this->montandoGrupo(array('pes_rg', 'pes_cpf'), 'grupo3');
        $this->montandoGrupo(array('pes_sexo'), 'grupo4');
        $this->montandoGrupo(array('pes_uf','pes_cidade'), 'grupo51');
        $this->montandoGrupo(array('u_email'), 'grupo5');
        $this->montandoGrupo(array('u_senha', 'u_senha2'), 'grupo6');
        
        
        
        
        $this->montandoGrupo(array('submit', 'limpar'), 'botoes');
    }

}