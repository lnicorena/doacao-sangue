<?php

class Doacao_Form_Unidade extends Utils_Form_Decorator_Default {

    public function init() {

        $this->setName('form-unidade');
        $this->setAttrib('class', 'zend_form form-horizontal');
        $this->setMethod('POST');

        $id = new Zend_Form_Element_Hidden('un_id');
        $id->addFilter('Int');
        $this->addElement($id);
        
        $idRev = new Zend_Form_Element_Hidden('rev_id');
        $idRev->addFilter('Int');
        $this->addElement($idRev);

        
        $UnSituacao = new Zend_Form_Element_Select('un_situacao');
        $UnSituacao->setLabel('Situação:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addMultiOptions(array('1' => ' Ativo', '0' => ' Inativo'));
        //$this->addElement($UnSituacao);
        
        $tipos = new Doacao_Model_Mapper_Tipounidade();
        $TunId = new Zend_Form_Element_Select('tun_id');
        $TunId->setLabel('Tipo de Unidade:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $TunId->addMultiOptions(array('' => ' Selecione'));
        foreach ($tipos->getTiposUnidade() as $t)
            $TunId->addMultiOptions(array($t->tun_id => $t->tun_nome));
        $this->addElement($TunId);
        

        $RevNregistro = new Zend_Form_Element_Text('rev_nregistro');
        $RevNregistro->setLabel('Nº Registro:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevNregistro);
        
        
        $RevCnpj = new Zend_Form_Element_Text('rev_cnpj');
        $RevCnpj->setLabel('CNPJ:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevCnpj);
        

        $RevNome = new Zend_Form_Element_Text('rev_nome');
        $RevNome->setLabel('Nome da unidade:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevNome);
        

        $RevDesc = new Zend_Form_Element_Textarea('rev_desc');
        $RevDesc->setLabel('Descrição:')
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->setAttrib('rows', '3');
        $this->addElement($RevDesc);
        

        $RevUf = new Zend_Form_Element_Text('rev_uf');
        $RevUf->setLabel('UF:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevUf);
        

        $RevCidade = new Zend_Form_Element_Text('rev_cidade');
        $RevCidade->setLabel('Cidade:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevCidade);
        

        $RevCep = new Zend_Form_Element_Text('rev_cep');
        $RevCep->setLabel('CEP:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevCep);
        

        $RevBairro = new Zend_Form_Element_Text('rev_bairro');
        $RevBairro->setLabel('Bairro:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevBairro);
        
        
        $RevRua = new Zend_Form_Element_Text('rev_rua');
        $RevRua->setLabel('Rua:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevRua);
        
        

        $RevNum = new Zend_Form_Element_Text('rev_num');
        $RevNum->setLabel('Nº:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');
        $this->addElement($RevNum);
        
        
        $RevObs = new Zend_Form_Element_Textarea('rev_obs');
        $RevObs->setLabel('Observações:')
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->setAttrib('rows', '2');
        $this->addElement($RevObs);
        


        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Salvar')
                ->setAttrib('class', 'btn btn-large btn-primary')
                ->setIgnore(true);
        $this->addElement($submit);

        $limpar = new Zend_Form_Element_Button('limpar');
        $limpar->setLabel('Limpar')
                ->setAttrib('type', 'reset')
                ->setAttrib('class', 'btn btn-large btn-warning')
                ->setIgnore(true);
        $this->addElement($limpar);

        
        
        $this->configurandoTamanho('rev_nome', 'span6');
        $this->configurandoTamanho('tun_id', 'span4');
        //$this->configurandoTamanho('un_situacao', 'span2');
        
        $this->configurandoTamanho('rev_nregistro', 'span4');
        $this->configurandoTamanho('rev_cnpj', 'span4');
        $this->configurandoTamanho('rev_desc', 'span12');
        
        $this->configurandoTamanho('rev_uf', 'span1');
        $this->configurandoTamanho('rev_cidade', 'span4');
        $this->configurandoTamanho('rev_cep', 'span3');
        $this->configurandoTamanho('rev_bairro', 'span4');
        
        $this->configurandoTamanho('rev_rua', 'span8');
        $this->configurandoTamanho('rev_num', 'span4');
        $this->configurandoTamanho('rev_obs', 'span12');
        
        
        $this->montandoGrupo(
            array(
                'rev_nome', 
                'tun_id',
                //'un_situacao'
                )
            , 'campos1'
        );
        
        $this->montandoGrupo( array( 'rev_nregistro', 'rev_cnpj') , 'campos2' );
        $this->montandoGrupo( array( 'rev_desc' ), 'campos3' );
        
        $this->montandoGrupo( array('rev_uf', 'rev_cidade','rev_cep','rev_bairro'), 'endereco' );
        $this->montandoGrupo( array('rev_rua', 'rev_num'), 'endereco2' );
        $this->montandoGrupo( array('rev_obs'), 'endereco3' );
        
        $this->montandoGrupo(array('submit', 'limpar'), 'botoes');
        
    }

}