<?php

class Doacao_PessoaController extends Zend_Controller_Action {

    public $feedback;
    private $id;
    private $model;
    private $mapper;
    private $form;
    
    public function init() {
        $this->id = intval($this->_getParam('id', 0));
        $this->feedback = $this->_helper->getHelper('FlashMessenger');
        $this->model = new Doacao_Model_Pessoa();
        $this->mapper = new Doacao_Model_Mapper_Pessoa();
        $this->form = new Doacao_Form_Pessoa();
    }

    public function indexAction() {
        $pagina = intval($this->_getParam('pagina', 1));
        $dados = $this->view->pessoa = $this->mapper->getPessoas();

        $paginacao = Zend_Paginator::factory($dados);
        $paginacao->setItemCountPerPage(Zend_Registry::get('itens_por_pagina'));
        $paginacao->setPageRange(Zend_Registry::get('range'));
        $paginacao->setCurrentPageNumber($pagina);
        $this->view->paginacao = $paginacao;
    }

    public function adicionarAction() {
        $this->form->submit->setLabel('Adicionar');
        
        if ($this->getRequest()->isPost()) {
            $formulario = $this->getRequest()->getPost();
            if ($this->form->isValid($formulario)) {
                try{
                                        $this->model->pes_nome = $this->form->getValue('pes_nome');
                                        $this->model->pes_apelido = $this->form->getValue('pes_apelido');
                                        $this->model->pes_rg = $this->form->getValue('pes_rg');
                                        $this->model->pes_cpf = $this->form->getValue('pes_cpf');
                                        $this->model->pes_sexo = $this->form->getValue('pes_sexo');
                                        $this->mapper->save($this->model);
                    
                    $this->feedback->addMessage('success'); $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi adicionado com sucesso!");                    
                        
                } catch (Exception $e){
                    $this->feedback->addMessage('error'); $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao inserir o registro. Detalhes: ".$e->getMessage());
                }
                $this->_helper->redirector('index');
            } else {
                $this->form->populate($formulario);
            }
        }
        
        $this->view->form = $this->form;
    }

    public function editarAction() {
        $id = intval($this->_getParam('id', 0));

        if ($id > 0) {
            $this->form->submit->setLabel('Salvar');
            $this->form->limpar->setAttrib('class','btn-voltar');

            if ($this->getRequest()->isPost()) {
                $formulario = $this->getRequest()->getPost();
                if ($this->form->isValid($formulario)) {
                    try{
                        $this->model = $this->mapper->getPessoaById($id);
                                                $this->model->pes_nome = $this->form->getValue('pes_nome');
                                                $this->model->pes_apelido = $this->form->getValue('pes_apelido');
                                                $this->model->pes_rg = $this->form->getValue('pes_rg');
                                                $this->model->pes_cpf = $this->form->getValue('pes_cpf');
                                                $this->model->pes_sexo = $this->form->getValue('pes_sexo');
                                                $this->mapper->save($this->model);
                        
                        $this->feedback->addMessage('success'); $this->feedback->addMessage('Sucesso');
                        $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi alterado com sucesso!");                    
                        
                    } catch (Exception $e){
                        $this->feedback->addMessage('error'); $this->feedback->addMessage('Erro');
                        $this->feedback->addMessage("Ocorreu um erro ao editar o registro. Detalhes: ".$e->getMessage());
                    }
                    
                    $this->_helper->redirector('index');
                } else {
                    $this->form->populate($formulario);
                }
            } else {
                $this->form->populate($this->mapper->getPessoaById($id, true));     
            }
            $this->view->form = $this->form;
        }
    }
    
    public function removerAction() {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            if ($del == 'Sim') {
                $id = $this->getRequest()->getPost('id');
                try{
                    $this->model = $this->mapper->getPessoaById($id);
                    $this->mapper->remove($this->model);
                    
                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso'); 
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi removido com sucesso!");
                }  catch (Exception $e){
                    $this->feedback->addMessage('error'); $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao tentar remover o registro. Detalhes: ".$e->getMessage());
                }
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id', 0);
            $this->view->data = $this->mapper->getPessoaById($id, true);
        }
    }    

}