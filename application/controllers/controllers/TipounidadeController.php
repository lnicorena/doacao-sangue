<?php

class Doacao_TipounidadeController extends Zend_Controller_Action {

    public $feedback;
    private $id;
    private $model;
    private $mapper;
    private $form;
    
    public function init() {
        $this->id = intval($this->_getParam('id', 0));
        $this->feedback = $this->_helper->getHelper('FlashMessenger');
        $this->model = new Doacao_Model_Tipounidade();
        $this->mapper = new Doacao_Model_Mapper_Tipounidade();
        $this->form = new Doacao_Form_Tipounidade();
    }

    public function indexAction() {
        $pagina = intval($this->_getParam('pagina', 1));
        $dados = $this->view->tipounidade = $this->mapper->getTipounidades();

        $paginacao = Zend_Paginator::factory($dados);
        $paginacao->setItemCountPerPage(Zend_Registry::get('itens_por_pagina'));
        $paginacao->setPageRange(Zend_Registry::get('range'));
        $paginacao->setCurrentPageNumber($pagina);
        $this->view->paginacao = $paginacao;
    }

    public function adicionarAction() {
        $this->form->submit->setLabel('Adicionar');
        
        if ($this->getRequest()->isPost()) {
            $formulario = $this->getRequest()->getPost();
            if ($this->form->isValid($formulario)) {
                try{
                                        $this->model->tun_nome = $this->form->getValue('tun_nome');
                                        $this->model->tun_desc = $this->form->getValue('tun_desc');
                                        $this->model->tun_param = $this->form->getValue('tun_param');
                                        $this->mapper->save($this->model);
                    
                    $this->feedback->addMessage('success'); $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi adicionado com sucesso!");                    
                        
                } catch (Exception $e){
                    $this->feedback->addMessage('error'); $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao inserir o registro. Detalhes: ".$e->getMessage());
                }
                $this->_helper->redirector('index');
            } else {
                $this->form->populate($formulario);
            }
        }
        
        $this->view->form = $this->form;
    }

    public function editarAction() {
        $id = intval($this->_getParam('id', 0));

        if ($id > 0) {
            $this->form->submit->setLabel('Salvar');
            $this->form->limpar->setAttrib('class','btn-voltar');

            if ($this->getRequest()->isPost()) {
                $formulario = $this->getRequest()->getPost();
                if ($this->form->isValid($formulario)) {
                    try{
                        $this->model = $this->mapper->getTipounidadeById($id);
                                                $this->model->tun_nome = $this->form->getValue('tun_nome');
                                                $this->model->tun_desc = $this->form->getValue('tun_desc');
                                                $this->model->tun_param = $this->form->getValue('tun_param');
                                                $this->mapper->save($this->model);
                        
                        $this->feedback->addMessage('success'); $this->feedback->addMessage('Sucesso');
                        $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi alterado com sucesso!");                    
                        
                    } catch (Exception $e){
                        $this->feedback->addMessage('error'); $this->feedback->addMessage('Erro');
                        $this->feedback->addMessage("Ocorreu um erro ao editar o registro. Detalhes: ".$e->getMessage());
                    }
                    
                    $this->_helper->redirector('index');
                } else {
                    $this->form->populate($formulario);
                }
            } else {
                $this->form->populate($this->mapper->getTipounidadeById($id, true));     
            }
            $this->view->form = $this->form;
        }
    }
    
    public function removerAction() {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            if ($del == 'Sim') {
                $id = $this->getRequest()->getPost('id');
                try{
                    $this->model = $this->mapper->getTipounidadeById($id);
                    $this->mapper->remove($this->model);
                    
                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso'); 
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi removido com sucesso!");
                }  catch (Exception $e){
                    $this->feedback->addMessage('error'); $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao tentar remover o registro. Detalhes: ".$e->getMessage());
                }
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id', 0);
            $this->view->data = $this->mapper->getTipounidadeById($id, true);
        }
    }    

}