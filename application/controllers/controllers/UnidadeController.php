<?php

class Doacao_UnidadeController extends Zend_Controller_Action {

    public $feedback;
    private $id;
    private $model;
    private $mapper;
    private $form;

    public function init() {
        $this->id = intval($this->_getParam('id', 0));
        $this->feedback = $this->_helper->getHelper('FlashMessenger');
        $this->model = new Doacao_Model_Unidade();
        $this->modelRev = new Doacao_Model_Unidaderev();
        $this->mapper = new Doacao_Model_Mapper_Unidade();
        $this->mapperRev = new Doacao_Model_Mapper_Unidaderev();
        $this->form = new Doacao_Form_Unidade();
    }

    public function indexAction() {
        $pagina = intval($this->_getParam('pagina', 1));
        $this->view->unidades = $this->mapper->getUnidades();
    }

    public function addAction() {
        $this->form->submit->setLabel('Adicionar');

        if ($this->getRequest()->isPost()) {
            $formulario = $this->getRequest()->getPost();
            if ($this->form->isValid($formulario)) {
                try {

                    $this->model->un_situacao = $this->form->getValue('un_situacao');
                    $this->model->un_modificacoes = $this->form->getValue('un_modificacoes');
                    $id = $this->mapper->save($this->model);

                    $this->modelRev->un_id = $this->form->getValue($id);
                    //$this->modelRev->u_id = $this->form->getValue(Zend_Registry::get('user')->u_id);
                    $this->modelRev->tun_id = $this->form->getValue('tun_id');
                    $this->modelRev->rev_timestamp = $this->form->getValue('rev_timestamp');
                    $this->modelRev->rev_nregistro = $this->form->getValue('rev_nregistro');
                    $this->modelRev->rev_cnpj = $this->form->getValue('rev_cnpj');
                    $this->modelRev->rev_nome = $this->form->getValue('rev_nome');
                    $this->modelRev->rev_desc = $this->form->getValue('rev_desc');
                    $this->modelRev->rev_uf = $this->form->getValue('rev_uf');
                    $this->modelRev->rev_cidade = $this->form->getValue('rev_cidade');
                    $this->modelRev->rev_cep = $this->form->getValue('rev_cep');
                    $this->modelRev->rev_rua = $this->form->getValue('rev_rua');
                    $this->modelRev->rev_num = $this->form->getValue('rev_num');
                    $this->modelRev->rev_obs = $this->form->getValue('rev_obs');
                    $this->modelRev->rev_lat = $this->form->getValue('rev_lat');
                    $this->modelRev->rev_lon = $this->form->getValue('rev_lon');
                    $this->mapperRev->save($this->modelRev);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi adicionado com sucesso!");
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao inserir o registro. Detalhes: " . $e->getMessage());
                }
                $this->_helper->redirector('index');
            } else {
                $this->form->populate($formulario);
            }
        }

        $this->view->form = $this->form;
    }

    public function editarAction() {
        $id = intval($this->_getParam('id', 0));

        if ($id > 0) {
            $this->form->submit->setLabel('Salvar');
            $this->form->limpar->setAttrib('class', 'btn-voltar');

            if ($this->getRequest()->isPost()) {
                $formulario = $this->getRequest()->getPost();
                if ($this->form->isValid($formulario)) {
                    try {
                        $this->model = $this->mapper->getUnidadeById($id);
                        $this->model->un_situacao = $this->form->getValue('un_situacao');
                        $this->model->un_modificacoes = $this->form->getValue('un_modificacoes');
                        $this->mapper->save($this->model);

                        $this->feedback->addMessage('success');
                        $this->feedback->addMessage('Sucesso');
                        $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi alterado com sucesso!");
                    } catch (Exception $e) {
                        $this->feedback->addMessage('error');
                        $this->feedback->addMessage('Erro');
                        $this->feedback->addMessage("Ocorreu um erro ao editar o registro. Detalhes: " . $e->getMessage());
                    }

                    $this->_helper->redirector('index');
                } else {
                    $this->form->populate($formulario);
                }
            } else {
                $this->form->populate($this->mapper->getUnidadeById($id, true));
            }
            $this->view->form = $this->form;
        }
    }

    public function removerAction() {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            if ($del == 'Sim') {
                $id = $this->getRequest()->getPost('id');
                try {
                    $this->model = $this->mapper->getUnidadeById($id);
                    $this->mapper->remove($this->model);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi removido com sucesso!");
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao tentar remover o registro. Detalhes: " . $e->getMessage());
                }
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id', 0);
            $this->view->data = $this->mapper->getUnidadeById($id, true);
        }
    }

}