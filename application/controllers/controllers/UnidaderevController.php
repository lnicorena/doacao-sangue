<?php

class Doacao_UnidaderevController extends Zend_Controller_Action {

    public $feedback;
    private $id;
    private $model;
    private $mapper;
    private $form;

    public function init() {
        $this->id = intval($this->_getParam('id', 0));
        $this->feedback = $this->_helper->getHelper('FlashMessenger');
        $this->model = new Doacao_Model_Unidaderev();
        $this->mapper = new Doacao_Model_Mapper_Unidaderev();
        $this->form = new Doacao_Form_Unidaderev();
    }

    public function indexAction() {
        $pagina = intval($this->_getParam('pagina', 1));
        $dados = $this->view->unidaderev = $this->mapper->getUnidaderevs();

        $paginacao = Zend_Paginator::factory($dados);
        $paginacao->setItemCountPerPage(Zend_Registry::get('itens_por_pagina'));
        $paginacao->setPageRange(Zend_Registry::get('range'));
        $paginacao->setCurrentPageNumber($pagina);
        $this->view->paginacao = $paginacao;
    }

    public function adicionarAction() {
        $this->form->submit->setLabel('Adicionar');

        if ($this->getRequest()->isPost()) {
            $formulario = $this->getRequest()->getPost();
            if ($this->form->isValid($formulario)) {
                try {
                    $this->model->tun_id = $this->form->getValue('tun_id');
                    $this->model->un_id = $this->form->getValue('un_id');
                    $this->model->u_id = $this->form->getValue('u_id');
                    $this->model->rev_timestamp = $this->form->getValue('rev_timestamp');
                    $this->model->rev_nregistro = $this->form->getValue('rev_nregistro');
                    $this->model->rev_cnpj = $this->form->getValue('rev_cnpj');
                    $this->model->rev_nome = $this->form->getValue('rev_nome');
                    $this->model->rev_desc = $this->form->getValue('rev_desc');
                    $this->model->rev_uf = $this->form->getValue('rev_uf');
                    $this->model->rev_cidade = $this->form->getValue('rev_cidade');
                    $this->model->rev_cep = $this->form->getValue('rev_cep');
                    $this->model->rev_rua = $this->form->getValue('rev_rua');
                    $this->model->rev_num = $this->form->getValue('rev_num');
                    $this->model->rev_obs = $this->form->getValue('rev_obs');
                    $this->model->rev_lat = $this->form->getValue('rev_lat');
                    $this->model->rev_lon = $this->form->getValue('rev_lon');
                    $this->mapper->save($this->model);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi adicionado com sucesso!");
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao inserir o registro. Detalhes: " . $e->getMessage());
                }
                $this->_helper->redirector('index');
            } else {
                $this->form->populate($formulario);
            }
        }

        $this->view->form = $this->form;
    }

    public function editarAction() {
        $id = intval($this->_getParam('id', 0));

        if ($id > 0) {
            $this->form->submit->setLabel('Salvar');
            $this->form->limpar->setAttrib('class', 'btn-voltar');

            if ($this->getRequest()->isPost()) {
                $formulario = $this->getRequest()->getPost();
                if ($this->form->isValid($formulario)) {
                    try {
                        $this->model = $this->mapper->getUnidaderevById($id);
                        $this->model->tun_id = $this->form->getValue('tun_id');
                        $this->model->un_id = $this->form->getValue('un_id');
                        $this->model->u_id = $this->form->getValue('u_id');
                        $this->model->rev_timestamp = $this->form->getValue('rev_timestamp');
                        $this->model->rev_nregistro = $this->form->getValue('rev_nregistro');
                        $this->model->rev_cnpj = $this->form->getValue('rev_cnpj');
                        $this->model->rev_nome = $this->form->getValue('rev_nome');
                        $this->model->rev_desc = $this->form->getValue('rev_desc');
                        $this->model->rev_uf = $this->form->getValue('rev_uf');
                        $this->model->rev_cidade = $this->form->getValue('rev_cidade');
                        $this->model->rev_cep = $this->form->getValue('rev_cep');
                        $this->model->rev_rua = $this->form->getValue('rev_rua');
                        $this->model->rev_num = $this->form->getValue('rev_num');
                        $this->model->rev_obs = $this->form->getValue('rev_obs');
                        $this->model->rev_lat = $this->form->getValue('rev_lat');
                        $this->model->rev_lon = $this->form->getValue('rev_lon');
                        $this->mapper->save($this->model);

                        $this->feedback->addMessage('success');
                        $this->feedback->addMessage('Sucesso');
                        $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi alterado com sucesso!");
                    } catch (Exception $e) {
                        $this->feedback->addMessage('error');
                        $this->feedback->addMessage('Erro');
                        $this->feedback->addMessage("Ocorreu um erro ao editar o registro. Detalhes: " . $e->getMessage());
                    }

                    $this->_helper->redirector('index');
                } else {
                    $this->form->populate($formulario);
                }
            } else {
                $this->form->populate($this->mapper->getUnidaderevById($id, true));
            }
            $this->view->form = $this->form;
        }
    }

    public function removerAction() {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            if ($del == 'Sim') {
                $id = $this->getRequest()->getPost('id');
                try {
                    $this->model = $this->mapper->getUnidaderevById($id);
                    $this->mapper->remove($this->model);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi removido com sucesso!");
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao tentar remover o registro. Detalhes: " . $e->getMessage());
                }
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id', 0);
            $this->view->data = $this->mapper->getUnidaderevById($id, true);
        }
    }

}