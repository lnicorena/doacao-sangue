<?php

class SolicitacaoController extends Zend_Controller_Action {

    public $feedback;
    private $id;
    private $model;
    private $mapper;
    private $form;

    public function init() {
        $this->id = intval($this->_getParam('id', 0));
        $this->feedback = $this->_helper->getHelper('FlashMessenger');
        $this->model = new Doacao_Model_Solicitacao();
        $this->mapper = new Doacao_Model_Mapper_Solicitacao();
        $this->form = new Doacao_Form_Solicitacao();
    }

    public function indexAction() {
        $pagina = intval($this->_getParam('pagina', 1));
        $dados = $this->view->solicitacao = $this->mapper->getSolicitacoes();

        $paginacao = Zend_Paginator::factory($dados);
        $paginacao->setItemCountPerPage(Zend_Registry::get('itens_por_pagina'));
        $paginacao->setPageRange(Zend_Registry::get('range'));
        $paginacao->setCurrentPageNumber($pagina);
        $this->view->paginacao = $paginacao;
    }

    public function visualizarAction() {

        try {
            if ($this->id > 0) {
                $this->view->sol = $this->mapper->getSolicitacoes(array('sol.sol_id = ?' => $this->id))->current();

                $soldoacMapper = new Doacao_Model_Mapper_Soldoador();
                $this->view->ndoacoes = $soldoacMapper->getSoldoadors("sol_id = {$this->id}", true);
            } else {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage("Não foi possível carregar as informações da unidade. Identificador inválido.");
                $this->_helper->redirector('index', 'index');
            }
        } catch (Exception $e) {
            Zend_Registry::get('logger')->log(array("Erro ao visualizar Unidade", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, $e), Utils_Log::_ERROR);
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage("Não foi possível carregar as informações da unidade. Se o problema persistir por favor entre em contato reportando o erro.");
            $this->_helper->redirector('index', 'index');
        }
    }

    public function doeiAction() {

        try {
            if ($this->id > 0) {
                $sol = $this->mapper->getSolicitacoes(array('sol.sol_id = ?' => $this->id))->current();

                $soldoacMapper = new Doacao_Model_Mapper_Soldoador();
                $uid = Zend_Registry::get('usuario')->u_id;
                if ($soldoacMapper->getSoldoadors("sol_id = {$this->id} AND u_id = {$uid}", true) > 0){
                    

                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Você já informou que doou sangue para esta solicitação.");
                }else{
                    $doacao = new Doacao_Model_Soldoador();
                    $doacao->u_id = Zend_Registry::get('usuario')->u_id;
                    $doacao->sol_id = $this->id;
                    $doacao->sd_timestamp = "NOW()";
                    $soldoacMapper->save($doacao);
                    
                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("Sua solicitação foi processada co, sucesso.");
                }
                $this->_helper->redirector('visualizar', 'solicitacao', 'default', array('id' => $this->id));
            } else {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage("Não foi possível carregar as informações da solicitacao. Identificador inválido.");
                $this->_helper->redirector('index', 'index');
            }
        } catch (Exception $e) {
            Zend_Registry::get('logger')->log(array("Erro ao visualizar Unidade", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, $e), Utils_Log::_ERROR);
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage("Não foi possível carregar as informações da unidade. Se o problema persistir por favor entre em contato reportando o erro.");
            $this->_helper->redirector('index');
        }
    }

    public function novoAction() {
        $this->form->submit->setLabel('Concluir');

        if ($this->getRequest()->isPost()) {
            $formulario = $this->getRequest()->getPost();

            if ($this->form->isValid($formulario)) {
                try {
                    $this->model->ts_cod = $this->form->getValue('ts_cod');
                    $this->model->tsol_cod = $this->form->getValue('tsol_cod');
                    $this->model->un_id = $this->form->getValue('un_id');
                    $this->model->u_id = Zend_Registry::get('usuario')->u_id;
                    $this->model->sol_dt = "NOW()";
                    $this->model->sol_qtd = (int) $this->form->getValue('sol_qtd');
                    $this->model->sol_paciente = $this->form->getValue('sol_paciente');
                    $this->model->sol_situacao = 1;
                    $id = $this->mapper->save($this->model);

                    Zend_Registry::get('logger')->log(array("Solicitação realizada.", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, $formulario), Utils_Log::_INFO);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("Solicitação realizada com sucesso!");
                    $this->_helper->redirector('visualizar', 'solicitacao', 'default', array('id' => $id));
                } catch (Exception $e) {
                    Zend_Registry::get('logger')->log(array("Erro solicitar doacao.", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, $formulario, $e), Utils_Log::_ERROR);
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao inserir o registro. Detalhes: " . $e->getMessage());
                    $this->_helper->redirector('index', 'index');
                }
            } else {
                $this->form->populate($formulario);
            }
        }
        $this->view->form = $this->form;
    }

    public function editarAction() {
        $id = intval($this->_getParam('id', 0));

        if ($id > 0) {
            $this->form->submit->setLabel('Salvar');
            $this->form->limpar->setAttrib('class', 'btn-voltar');

            if ($this->getRequest()->isPost()) {
                $formulario = $this->getRequest()->getPost();
                if ($this->form->isValid($formulario)) {
                    try {
                        $this->model = $this->mapper->getSolicitacaoById($id);
                        $this->model->ts_cod = $this->form->getValue('ts_cod');
                        $this->model->tsol_cod = $this->form->getValue('tsol_cod');
                        $this->model->un_id = $this->form->getValue('un_id');
                        $this->model->sol_qtd = (int) $this->form->getValue('sol_qtd');
                        $this->model->sol_paciente = $this->form->getValue('sol_paciente');
                        $this->mapper->save($this->model);

                        Zend_Registry::get('logger')->log(array("Solicitação alterada.", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, $formulario), Utils_Log::_INFO);

                        $this->feedback->addMessage('success');
                        $this->feedback->addMessage('Sucesso');
                        $this->feedback->addMessage("Solicitação alterada com sucesso!");
                        $this->_helper->redirector('visualizar', 'solicitacao', 'default', array('id' => $id));
                    } catch (Exception $e) {
                        $this->feedback->addMessage('error');
                        $this->feedback->addMessage('Erro');
                        $this->feedback->addMessage("Ocorreu um erro ao editar o registro. Detalhes: " . $e->getMessage());
                    }

                    $this->_helper->redirector('index');
                } else {
                    $this->form->populate($formulario);
                }
            } else {
                $this->form->populate($this->mapper->getSolicitacaoById($id, true));
            }
            $this->view->form = $this->form;
        }
    }

    public function removerAction() {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            if ($del == 'Sim') {
                $id = $this->getRequest()->getPost('id');
                try {
                    $this->model = $this->mapper->getSolicitacaoById($id);
                    $this->mapper->remove($this->model);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi removido com sucesso!");
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao tentar remover o registro. Detalhes: " . $e->getMessage());
                }
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id', 0);
            $this->view->data = $this->mapper->getSolicitacaoById($id, true);
        }
    }

}