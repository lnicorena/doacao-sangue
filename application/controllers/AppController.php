<?php

class AppController extends Zend_Controller_Action {

    private $facebook;
    private $user_id;
    private $umapper;
    private $fbmapper;
    private $pmapper;

    public function init() {
        $this->umapper = new Doacao_Model_Mapper_Usuario();
        $this->fbmapper = new Doacao_Model_Mapper_Facebook();
        $this->pmapper = new Doacao_Model_Mapper_Pessoa();


        $this->facebook = new API_Facebook(array(
            'appId' => Zend_Registry::get('fb')->app_id,
            'secret' => Zend_Registry::get('fb')->app_secret,
        ));


        $this->user_id = $this->facebook->getUser();
        
        $this->logaUsuario();
    }

    public function indexAction() {
        //$this->_helper->layout->disableLayout();
        $this->view->facebook = $this->facebook;
    }

    public function logaUsuario() {
//        $auth = Zend_Auth::getInstance();
//        $auth->clearIdentity();
        
        if ($this->user_id) {
            try {
                $user_profile = $this->facebook->api('/me');
                if ($user = $this->umapper->getUserByEmail($user_profile['email'])) {
                    $this->umapper->login($user->u_email, $user->fb_id, 0, 1);
                } elseif ($user = $this->umapper->getUserByFId($user_profile['id'])) {
                    $this->umapper->login($user->u_email, $user->fb_id, 0, 1);
                } else {
                    $this->primeiroAcesso(json_decode(json_encode($user_profile), FALSE));
                }
            } catch (FacebookApiException $e) {

                Zend_Registry::get('logger')->log($e, Utils_Log::_FB_API_ERROR);
                $this->user_id = null;
            }
        }

        $this->_helper->redirector('index', 'app');
    }

    public function primeiroAcesso($user_profile) {
        $usuario = new Doacao_Model_Usuario();
        $fbmodel = new Doacao_Model_Facebook();
        $pessoa = new Doacao_Model_Pessoa();

        $this->umapper->getDbTable()->getDefaultAdapter()->beginTransaction();

        $fbmodel->fb_id = $user_profile->id;
        $fbmodel->fb_username = $user_profile->username;
        $this->fbmapper->save($fbmodel);

        $usuario->u_email = $user_profile->email;
        $usuario->u_senha = null;
        $usuario->u_dt_cad = "NOW()";
        $usuario->u_situacao = 1;
        $usuario->u_tipo = 1;
        $usuario->fb_id = $user_profile->id;
        $id = $this->umapper->save($usuario);

        $pessoa->u_id = $id;
        $pessoa->pes_nome = $user_profile->name;
        $pessoa->pes_apelido = $user_profile->first_name;
        if ($user_profile->gender == 'male')
            $sexo = 'M';
        elseif ($user_profile->gender == 'female')
            $sexo = 'F';
        else
            $sexo = null;
        $pessoa->pes_sexo = $sexo;
        $this->pmapper->save($pessoa);

        $this->umapper->getDbTable()->getDefaultAdapter()->commit();

        $this->umapper->login($usuario->u_email, $usuario->fb_id, 0, 1);
    }

}

?>
