<?php

class UnidadeController extends Zend_Controller_Action {

    public $feedback;
    private $id;
    private $model;
    private $mapper;
    private $mapperRev;
    private $form;

    public function init() {
        $this->id = intval($this->_getParam('id', 0));
        $this->feedback = $this->_helper->getHelper('FlashMessenger');
        $this->model = new Doacao_Model_Unidade();
        $this->modelRev = new Doacao_Model_Unidaderev();
        $this->mapper = new Doacao_Model_Mapper_Unidade();
        $this->mapperRev = new Doacao_Model_Mapper_Unidaderev();
        $this->form = new Doacao_Form_Unidade();
    }

    public function indexAction() {

        $busca = $this->view->busca = $this->_getParam('busca');
        $tbusca = $this->view->tipo_busca = $this->_getParam('tipo_busca');

        if ($busca && $tbusca) {
            $where = array();
            switch ($tbusca) {
                case "cidade" : $where['rev.rev_cidade ILIKE ?'] = $busca;
                    break;
                case "uf" : $where['rev.rev_uf ILIKE ?'] = $busca;
                    break;
                case "nome" : $where['rev.rev_nome ILIKE ?'] = $busca;
                    break;
                default: $where = null;
            }

            $dados = $this->mapper->getUnidades($where);
        } else {
            $dados = $this->mapper->getUnidades();
        }


        $pagina = intval($this->_getParam('pagina', 1));
        $paginacao = Zend_Paginator::factory($dados);
        $paginacao->setItemCountPerPage(Zend_Registry::get('itens_por_pagina'));
        $paginacao->setPageRange(Zend_Registry::get('range'));
        $paginacao->setCurrentPageNumber($pagina);
        $this->view->unidades = $paginacao;
    }

    public function visualizarAction() {

        try {
            if ($this->id > 0) {

                $where = array('un.un_id = ?' => $this->id);
                $this->view->unidade = $this->mapper->getUnidades($where, null, 1)->current();
            } else {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage("Não foi possível carregar as informações da unidade. Identificador inválido.");
                $this->_helper->redirector('index');
            }
        } catch (Exception $e) {
            Zend_Registry::get('logger')->log(array("Erro ao visualizar Unidade", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, $e), Utils_Log::_ERROR);
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage("Não foi possível carregar as informações da unidade. Se o problema persistir por favor entre em contato reportando o erro.");
            $this->_helper->redirector('index');
        }
    }

    public function denunciarAction() {
        if (!isset(Zend_Registry::get('usuario')->u_id)) {
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage("É necessário estar logado no sistema para realizar esta ação.");
            $this->_helper->redirector('index');
        }

        try {

            if ($this->id > 0) {

                $where = array('rev.rev_id = ?' => $this->id);
                $un = $this->view->unidade = $this->mapper->getUnidades($where, null, 1)->current();

                $mapperUsuario = new Doacao_Model_Mapper_Usuario();
                $this->view->usuario = $mapperUsuario->getUsuarios("u.u_id = {$un->u_id}")->current();

                $form = new Doacao_Form_Denuncia($un->u_id);

                if ($this->getRequest()->isPost()) {
                    $mapperDen = new Doacao_Model_Mapper_Denuncia();
                    $denuncia = new Doacao_Model_Denuncia();
                    $denuncia->un_rev_id = $this->id;
                    $denuncia->tden_cod = $this->getRequest()->getPost('tden_cod');
                    $denuncia->u_id_denunciado = $this->getRequest()->getPost('u_id_denunciado');
                    $denuncia->u_id = Zend_Registry::get('usuario')->u_id;
                    $denuncia->den_timestamp = "NOW()";
                    $denuncia->den_situacao = 0;
                    $denuncia->den_text = $this->getRequest()->getPost('den_text');
                    $mapperDen->save($denuncia);

                    Zend_Registry::get('logger')->log(array("Denuncia realizada.", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, $denuncia), Utils_Log::_INFO);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Informação');
                    $this->feedback->addMessage("Sua solicitação foi processeada e em breve estará sendo analisada.");
                    $this->_helper->redirector('index', 'index');
                }
                $this->view->form = $form;
            } else {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage("Não foi possível processar a solicitação. Identificador inválido.");
                $this->_helper->redirector('index');
            }
        } catch (Exception $e) {
            Zend_Registry::get('logger')->log(array("Erro ao denunciar Unidade.", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, "UnidadeRev:" . $this->id, $e), Utils_Log::_ERROR);
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage("Não foi possível carregar as informações da unidade. Se o problema persistir por favor entre em contato reportando o erro.");
            $this->_helper->redirector('index');
        }
    }

    public function addAction() {
        $this->form->submit->setLabel('Adicionar');

        if ($this->getRequest()->isPost()) {
            $formulario = $this->getRequest()->getPost();
            if ($this->form->isValid($formulario)) {
                try {

                    $this->mapper->getDbTable()->getDefaultAdapter()->beginTransaction();

                    $this->model->un_situacao = 1;
                    $this->model->un_modificacoes = 1;
                    $id = $this->mapper->save($this->model);

                    $this->modelRev->un_id = $id;
                    $this->modelRev->u_id = Zend_Registry::get('usuario')->u_id;
                    $this->modelRev->tun_id = $this->form->getValue('tun_id');
                    $this->modelRev->rev_nregistro = $this->form->getValue('rev_nregistro');
                    $this->modelRev->rev_cnpj = $this->form->getValue('rev_cnpj');
                    $this->modelRev->rev_nome = $this->form->getValue('rev_nome');
                    $this->modelRev->rev_desc = $this->form->getValue('rev_desc');
                    $this->modelRev->rev_uf = $this->form->getValue('rev_uf');
                    $this->modelRev->rev_cidade = $this->form->getValue('rev_cidade');
                    $this->modelRev->rev_cep = $this->form->getValue('rev_cep');
                    $this->modelRev->rev_bairro = $this->form->getValue('rev_bairro');
                    $this->modelRev->rev_rua = $this->form->getValue('rev_rua');
                    $this->modelRev->rev_num = $this->form->getValue('rev_num');
                    $this->modelRev->rev_obs = $this->form->getValue('rev_obs');
                    $this->modelRev->rev_lat = $this->form->getValue('rev_lat');
                    $this->modelRev->rev_lon = $this->form->getValue('rev_lon');
                    $this->mapperRev->save($this->modelRev);

                    $this->mapper->getDbTable()->getDefaultAdapter()->commit();

                    Zend_Registry::get('logger')->log(array("Unidade inserida.", "un_id:$id", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome), Utils_Log::_INFO);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi adicionado com sucesso!");
                } catch (Exception $e) {
                    Zend_Registry::get('logger')->log(array("Erro ao inserir Unidade", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome, $formulario, $e), Utils_Log::_ERROR);
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao inserir o registro. Detalhes: " . $e->getMessage());
                }
                $this->_helper->redirector('index');
            } else {
                $this->form->populate($formulario);
            }
        }

        $this->view->form = $this->form;
    }

    public function editarAction() {
        $id = intval($this->_getParam('id', 0));

        if ($id > 0) {
            $this->form->submit->setLabel('Salvar');
            $this->form->limpar->setAttrib('class', 'btn-voltar');

            if ($this->getRequest()->isPost()) {
                $formulario = $this->getRequest()->getPost();
                if ($this->form->isValid($formulario)) {
                    try {
                        $this->mapper->getDbTable()->getDefaultAdapter()->beginTransaction();

                        $this->model = $this->mapper->getUnidadeById($id);
                        $this->model->un_modificacoes ++;
                        $this->mapper->save($this->model);

                        $this->modelRev->un_id = $id;
                        $this->modelRev->u_id = Zend_Registry::get('usuario')->u_id;
                        $this->modelRev->tun_id = $this->form->getValue('tun_id');
                        $this->modelRev->rev_nregistro = $this->form->getValue('rev_nregistro');
                        $this->modelRev->rev_cnpj = $this->form->getValue('rev_cnpj');
                        $this->modelRev->rev_nome = $this->form->getValue('rev_nome');
                        $this->modelRev->rev_desc = $this->form->getValue('rev_desc');
                        $this->modelRev->rev_uf = $this->form->getValue('rev_uf');
                        $this->modelRev->rev_cidade = $this->form->getValue('rev_cidade');
                        $this->modelRev->rev_cep = $this->form->getValue('rev_cep');
                        $this->modelRev->rev_bairro = $this->form->getValue('rev_bairro');
                        $this->modelRev->rev_rua = $this->form->getValue('rev_rua');
                        $this->modelRev->rev_num = $this->form->getValue('rev_num');
                        $this->modelRev->rev_obs = $this->form->getValue('rev_obs');
                        $this->modelRev->rev_lat = $this->form->getValue('rev_lat');
                        $this->modelRev->rev_lon = $this->form->getValue('rev_lon');
                        $this->mapperRev->save($this->modelRev);

                        $this->mapper->getDbTable()->getDefaultAdapter()->commit();

                        Zend_Registry::get('logger')->log(array("Unidade inserida.", "un_id:$id", "usuario:" . Zend_Registry::get('usuario')->u_id . Zend_Registry::get('usuario')->pes_nome), Utils_Log::_INFO);

                        $this->feedback->addMessage('success');
                        $this->feedback->addMessage('Sucesso');
                        $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi alterado com sucesso!");
                    } catch (Exception $e) {
                        $this->feedback->addMessage('error');
                        $this->feedback->addMessage('Erro');
                        $this->feedback->addMessage("Ocorreu um erro ao editar o registro. Detalhes: " . $e->getMessage());
                    }

                    $this->_helper->redirector('index');
                } else {
                    $this->form->populate($formulario);
                }
            } else {
                $unidade = $this->mapper->getUnidades("un.un_id = $id", null, 1)->current()->toArray();
                $this->form->populate($unidade);
            }
            $this->view->form = $this->form;
        } else {
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage("Não foi possível carregar as informações da unidade. Identificador inválido.");
            $this->_helper->redirector('index');
        }
    }

    public function removerAction() {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            if ($del == 'Sim') {
                $id = $this->getRequest()->getPost('id');
                try {
                    $this->model = $this->mapper->getUnidadeById($id);
                    $this->mapper->remove($this->model);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi removido com sucesso!");
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao tentar remover o registro. Detalhes: " . $e->getMessage());
                }
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id', 0);
            $this->view->data = $this->mapper->getUnidadeById($id, true);
        }
    }

}