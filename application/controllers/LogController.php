<?php

class LogController extends Zend_Controller_Action {

    public $feedback;
    private $id;
    private $model;
    private $mapper;
    private $form;

    public function init() {
        $this->id = intval($this->_getParam('id', 0));
        $this->feedback = $this->_helper->getHelper('FlashMessenger');
        $this->model = new Doacao_Model_Log();
        $this->mapper = new Doacao_Model_Mapper_Log();
    }

    public function indexAction() {
        $pagina = intval($this->_getParam('pagina', 1));
        $dados = $this->view->logs = $this->mapper->getLogs();

        $paginacao = Zend_Paginator::factory($dados);
        $paginacao->setItemCountPerPage(Zend_Registry::get('itens_por_pagina'));
        $paginacao->setPageRange(Zend_Registry::get('range'));
        $paginacao->setCurrentPageNumber($pagina);
        $this->view->paginacao = $paginacao;
    }

    public function removerAction() {
        if ($this->getRequest()->isPost()) {
            $del = $this->getRequest()->getPost('del');
            if ($del == 'Sim') {
                $id = $this->getRequest()->getPost('id');
                try {
                    $this->model = $this->mapper->getLogById($id);
                    $this->mapper->remove($this->model);

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("O registro <strong>{$this->model->nome}</strong> foi removido com sucesso!");
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("Ocorreu um erro ao tentar remover o registro. Detalhes: " . $e->getMessage());
                }
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id', 0);
            $this->view->data = $this->mapper->getLogById($id, true);
        }
    }

}