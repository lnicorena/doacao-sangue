<?php

class UsuarioController extends Zend_Controller_Action {

    protected $auth = null;
    public $feedback;
    private $id;
    protected $model;
    protected $modelFb;
    protected $modelPes;
    protected $mapper;
    protected $mapperFb;
    protected $mapperPes;
    protected $form;

    public function init() {

        $this->auth = Zend_Auth::getInstance();
        $this->feedback = $this->_helper->getHelper('FlashMessenger');

        $this->id = intval($this->_getParam('id', 0));

        $this->model = new Doacao_Model_Usuario();
        $this->modelFb = new Doacao_Model_Facebook();
        $this->modelPes = new Doacao_Model_Pessoa();

        $this->mapper = new Doacao_Model_Mapper_Usuario();
        $this->mapperFb = new Doacao_Model_Mapper_Facebook();
        $this->mapperPes = new Doacao_Model_Mapper_Pessoa();

        $this->form = new Doacao_Form_Usuario();
    }

    public function registrarAction() {
        $this->form->submit->setLabel('Adicionar');

        if ($this->getRequest()->isPost()) {
            $formulario = $this->getRequest()->getPost();
            if ($this->form->isValid($formulario)) {
                try {

                    $senha = $this->form->getValue('u_senha');
                    $senha2 = $this->form->getValue('u_senha2');
                    $email = $this->form->getValue('u_email');

                    if (is_null($senha) || $senha == "" || is_null($senha2) || $senha2 == "" || $senha !== $senha2)
                        throw new Exception("As senhas digitadas não conferem.");

                    if ($this->mapper->emailExists($email))
                        throw new Exception("O email informado já está cadastrado. Clique em \"esqueci minha senha\" ou faça o login pelo facebook.");


                    $this->mapper->getDbTable()->getDefaultAdapter()->beginTransaction();

                    $this->model->u_email = $email;
                    $this->model->u_senha = md5($senha);
                    $this->model->u_dt_cad = "NOW()";
                    $this->model->u_situacao = 0;
                    $this->model->u_tipo = 1;
                    $this->model->u_hash = md5(time());
                    $id = $this->mapper->save($this->model);

                    $this->modelPes->u_id = $id;
                    $this->modelPes->pes_nome = $this->form->getValue('pes_nome');
                    $this->modelPes->pes_apelido = $this->form->getValue('pes_apelido');
                    $this->modelPes->pes_rg = $this->form->getValue('pes_rg');
                    $this->modelPes->pes_cpf = $this->form->getValue('pes_cpf');
                    $this->modelPes->pes_sexo = $this->form->getValue('pes_sexo');
                    $this->modelPes->pes_uf = $this->form->getValue('pes_uf');
                    $this->modelPes->pes_cidade = $this->form->getValue('pes_cidade');
                    $this->modelPes->pes_lat = $this->form->getValue('pes_lat');
                    $this->modelPes->pes_lon = $this->form->getValue('pes_lon');
                    $this->mapperPes->save($this->modelPes);

                    $this->mapper->getDbTable()->getDefaultAdapter()->commit();

                    // @TODO enviar email com o hash

                    $this->feedback->addMessage('success');
                    $this->feedback->addMessage('Sucesso');
                    $this->feedback->addMessage("Registro concluído com sucesso!");
                } catch (Zend_Db_Adapter_Exception $dbe) {
                    $this->mapper->getDbTable()->getDefaultAdapter()->rollBack();
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Ocorreu um erro');
                    $this->feedback->addMessage("Ocorreu um erro ao concluir o registro. Tente novamente, se o erro persistir entre em contato com o suporte.");
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Ocorreu um erro');
                    $this->feedback->addMessage($e->getMessage());
                    $this->_helper->redirector('registrar');
                }
                $this->_helper->redirector('index', 'index');
            } else {
                $this->form->populate($formulario);
            }
        }

        $this->view->form = $this->form;
    }

    public function indexAction() {
        //$this->_helper->layout->disableLayout();

        if ($this->getRequest()->isPost()) {

            $usuario = $this->getRequest()->getPost('usuario');
            $senha = $this->getRequest()->getPost('senha');
            $lembrar = $this->getRequest()->getPost('manter') ? 1 : 0;

            $acesso = $this->mapper->login($usuario, $senha, $lembrar);

            if ($acesso === true) {
                $this->_redirect('/');
            } elseif ($acesso === -1) {
                $this->view->erro = 'Seu usuário encontra-se inativo. Entre em contato com o administrador do sistema.';
            } else {
                $this->view->erro = 'O usuário ou a senha inserido está incorreto.';
            }
        }
    }

    public function facebookAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        try {
            $token = $this->getRequest()->getParam('token', false);
            if ($token == false) {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Ocorreu um erro');
                $this->feedback->addMessage('Não foi possível entrar com o usuário do facebook.');
                $this->_redirect('/');
            }

            $graph_url = "https://graph.facebook.com/me?access_token=" . $token;

            $details = json_decode(file_get_contents($graph_url));

            if ($user = $this->mapper->getUserByEmail($details->email)) {
                $this->mapper->login($user->u_email, $user->fb_id, 0, 1);
            } elseif ($user = $this->mapper->getUserByFId($details->id)) {
                $this->mapper->login($user->u_email, $user->fb_id, 0, 1);
            } else {

                // Primeiro acesso
                $this->mapper->getDbTable()->getDefaultAdapter()->beginTransaction();

                $this->modelFb->fb_id = $details->id;
                $this->modelFb->fb_username = $details->username;
                $this->mapperFb->save($this->modelFb);

                $this->model->u_email = $details->email;
                $this->model->u_senha = null;
                $this->model->u_dt_cad = "NOW()";
                $this->model->u_situacao = 1;
                $this->model->u_tipo = 1;
                $this->model->fb_id = $details->id;
                $id = $this->mapper->save($this->model);

                $this->modelPes->u_id = $id;
                $this->modelPes->pes_nome = $details->name;
                $this->modelPes->pes_apelido = $details->first_name;
                if ($details->gender == 'male')
                    $sexo = 'M';
                elseif ($details->gender == 'female')
                    $sexo = 'F';
                else
                    $sexo = null;
                $this->modelPes->pes_sexo = $sexo;
                $this->mapperPes->save($this->modelPes);

                $this->mapper->getDbTable()->getDefaultAdapter()->commit();

                $this->mapper->login($this->model->u_email, $this->model->fb_id, 0, 1);
            }
        } catch (Exception $e) {
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Ocorreu um erro');
            $this->feedback->addMessage('Não foi possível efetuar o login. Tente novamente e se o problema persistir entre em contato com o suporte.');
        }
        $this->_redirect('/');
    }

    public function logoffAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->auth->clearIdentity();
        //unset($_SESSION['_acl']);
        Zend_Session::forgetMe();
        $this->_redirect('/');
    }

    public function perfilAction() {
        $this->_helper->layout->enableLayout();
        $form = new Cadastro_Form_Pessoa();
        $form->submit->setLabel('Salvar');
        $form->limpar->setAttrib('class', 'btn-voltar');

        $form->removeElements(
                array('pes_sexo',
                    'empresa_id',
                    'pes_cpf',
                    'pes_ativo',
                    'perfil',
                    'pes_cod1',
                    'pes_pass2',
                    'locais',
                    'areas')
        );

        $form->removeDisplayGroup('permissoes');

        $form->pes_pass->setLabel('Senha atual');
        $form->pes_pass->setRequired(false)
                ->removeFilter('StripTags')
                ->removeFilter('StringTrim');

//        $form->pes_pass2->setRequired(false)
//                ->removeFilter('StripTags')
//                ->removeFilter('StringTrim');


        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {
                try {
                    $pessoa_mapper = new Cadastro_Model_Mapper_Pessoa();


                    if ($form->getValue('pes_pass') != '' && $pessoa_mapper->validaSenhaAtual($form->getValue('pes_id'), $form->getValue('pes_pass'))) {

                        $pessoa = $pessoa_mapper->getPessoaById($form->getValue('pes_id'));
                        $pessoa->pes_ncompleto = $form->getValue('pes_ncompleto');
                        $pessoa->pes_nresumido = $form->getValue('pes_nresumido');
                        $pessoa->pes_email = $form->getValue('pes_email');

                        $snova = $form->getValue('pes_pass_new');
                        $snova2 = $form->getValue('pes_pass_new2');

                        if (isset($snova) && $snova != "" && isset($snova2) && $snova2 != "") {
                            if ($snova == $snova2) {
                                $pessoa->pes_pass = md5($snova);
                            } else {
                                $this->feedback->addMessage('error');
                                $this->feedback->addMessage('Erro');
                                $this->feedback->addMessage("As senhas informadas não conferem.");
                                $this->_helper->redirector('perfil');
                            }
                        }


                        /* upload da foto */
                        $foto = (isset($_FILES["pes_foto"]) && $_FILES["pes_foto"]["type"] != NULL) ? $_FILES["pes_foto"] : false;
                        $erro = array();

                        if ($foto) {
                            if (!eregi("^image\/(pjpeg|jpeg|png|gif|bmp)$", $foto["type"])) {
                                $erro[] = "Arquivo em formato inválido! A imagem deve ser jpg, jpeg, bmp, gif ou png. Envie outro arquivo";
                            } else {

                                if ($foto["size"] > $this->config["tamanho"]) {
                                    $erro[] = "Arquivo em tamanho muito grande! A imagem deve ser de no máximo " . $this->config["tamanho"] / 1024 . " Kb. Envie outro arquivo";
                                }

                                $tamanhos = getimagesize($foto["tmp_name"]);

                                if ($tamanhos[0] > $this->config["largura"]) {
                                    $erro[] = "Largura da imagem não deve ultrapassar " . $this->config["largura"] . " pixels";
                                }
                                if ($tamanhos[1] > $this->config["altura"]) {
                                    $erro[] = "Altura da imagem não deve ultrapassar " . $this->config["altura"] . " pixels";
                                }
                            }
                            if (sizeof($erro)) {
                                $msg = "";
                                foreach ($erro as $err) {
                                    $msg .= " - " . $err . "<br />";
                                }
                                $this->feedback->addMessage('error');
                                $this->feedback->addMessage('Erro');
                                $this->feedback->addMessage($msg);
                                $this->_helper->redirector('perfil', 'usuario', 'default');
                            } else {


                                preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

                                $imagem_nome = md5(uniqid(time())) . "." . $ext[1];

                                if ($pessoa->pes_foto != "") {
                                    @unlink($this->diretorio . $pessoa->pes_foto);
                                }

                                if (move_uploaded_file($foto["tmp_name"], $this->diretorio . $imagem_nome))
                                    $pessoa->pes_foto = $imagem_nome;
                                else
                                    $erroimg = true;
                            }
                        }


                        $pessoa_mapper->save($pessoa);
                        $this->feedback->addMessage('success');
                        $this->feedback->addMessage('Sucesso');
                        $this->feedback->addMessage("Seu perfil foi salvo com sucesso!");
                    } else {
                        $this->feedback->addMessage('error');
                        $this->feedback->addMessage('Erro');
                        $this->feedback->addMessage("A senha é incorreta.");
                        $this->_helper->redirector('perfil');
                    }
                } catch (Exception $e) {
                    $this->feedback->addMessage('error');
                    $this->feedback->addMessage('Erro');
                    $this->feedback->addMessage("<pre>{$e->getMessage()}</pre>");
                }

                $this->_helper->redirector(null, null, 'default');
            } else {
                $form->populate($formData);
            }
        } else {
            $pessoa_mapper = new Cadastro_Model_Mapper_Pessoa();
            $pessoa = $pessoa_mapper->getPessoaById(Zend_Registry::get('usuario')->pes_id, true);
            $this->view->foto = ($pessoa['pes_foto'] != "") ? $pessoa['pes_foto'] : "noimage.gif";
            $form->populate($pessoa);
        }

        $this->view->formulario = $form;
    }

}

