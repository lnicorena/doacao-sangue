<?php

class IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {

        try {
            $smapper = new Doacao_Model_Mapper_Solicitacao();
            $this->view->solicitacoes = $smapper->getSolicitacoes(null,10);
            
        } catch (Exception $e) {
            Zend_Registry::get('logger')->log(array("Erro ao visualizar página inicial", $e), Utils_Log::_ERROR);
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage("Não foi possível carregar as informações. Se o problema persistir por favor entre em contato reportando o erro.");
            $this->_helper->redirector('index', 'contato');
        }
    }

    public function emailAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        // Logger
        Zend_Registry::get('logger')->log(array('Teste2', 'Exemplo de mensagem de log'));
        die;

        // Sending email
        var_dump(
                Zend_Registry::get('notifica')->email('lnicorena@gmail.com', 'contato', "Há!", "<p>Teste <ul><li>áé´r´fd´sad´;.as´d!@#$%¨&*</li></ul><p>")
        );
    }

    public function queueAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $options = array(
            'name' => 'queue1',
        );

        // Create an array queue
        $queue = new Zend_Queue('Array', $options);

        // Get list of queues
        foreach ($queue->getQueues() as $name) {
            echo $name, "\n";
        }

        // Create a new queue
        $queue2 = $queue->createQueue('queue2');

        // Get number of messages in a queue (supports Countable interface from SPL)
        echo count($queue);

        // Get up to 5 messages from a queue
        $messages = $queue->receive(5);

        foreach ($messages as $i => $message) {
            echo $message->body, "\n";

            // We have processed the message; now we remove it from the queue.
            $queue->deleteMessage($message);
        }

        // Send a message to the currently active queue
        $queue->send('My Test Message');

        // Delete a queue we created and all of it's messages
        $queue->deleteQueue('queue2');
    }

}

