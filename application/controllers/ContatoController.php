<?php

class ContatoController extends Zend_Controller_Action {

    public $feedback;
    private $id;
    private $model;
    private $mapper;

    public function init() {
        $this->id = intval($this->_getParam('id', 0));
        $this->feedback = $this->_helper->getHelper('FlashMessenger');
        $this->model = new Doacao_Model_Contato();
        $this->mapper = new Doacao_Model_Mapper_Contato();
    }

    public function indexAction() {

        if ($this->getRequest()->isPost()) {
            $dados = $this->getRequest()->getPost();
            // @TODO adcionar recaptcha
            try {
                if (!isset($dados['nome']) || $dados['nome'] == "" || strlen($dados['nome']) < 5)
                    throw new Exception("O nome não foi informado ou é inválido.");

                if (!isset($dados['email']) || $dados['email'] == "" || strlen($dados['email']) < 5)
                    throw new Exception("O email não foi informado ou é inválido.");

                if (!isset($dados['assunto']) || $dados['assunto'] == "" || strlen($dados['assunto']) < 3)
                    throw new Exception("O assunto não foi informado ou é inválido.");

                if (!isset($dados['msg']) || $dados['msg'] == "" || strlen($dados['msg']) < 5)
                    throw new Exception("O conteúdo da mensagem não foi informado ou é muito curto.");

                $this->model->con_nome = $dados['nome'];
                $this->model->con_assunto = $dados['assunto'];
                $this->model->con_email = $dados['email'];
                $this->model->cont_msg = $dados['msg'];
                $this->model->con_ip = Zend_Registry::get('logger')->getIp();
                $this->model->con_timestamp = "NOW()";
                $this->model->con_uid = isset(Zend_Registry::get('usuario')->u_id) ? Zend_Registry::get('usuario')->u_id : null;
                $this->model->con_status = 1;
                $this->mapper->save($this->model);

                $this->feedback->addMessage('success');
                $this->feedback->addMessage('Concluído');
                $this->feedback->addMessage("Sua mensagem foi enviada. Em breve entraremos em contato.");
                $this->_helper->redirector('index', 'index', 'index');
            } catch (Zend_Db_Table_Exception $dbe) {
                Zend_Registry::get('logger')->log(array_merge($dados, $dbe), Utils_Log::_ERROR);
            } catch (Exception $e) {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Ocorreu um erro');
                $this->feedback->addMessage($e->getMessage());
                $_SESSION['contato'] = $dados;
                $this->_helper->redirector('index');
            }
        }
    }

    public function mensagensAction() {
        if (!isset(Zend_Registry::get('usuario')->u_tipo) || Zend_Registry::get('usuario')->u_tipo != Utils_ACL::_SUPERUSUARIO) {
            $this->feedback = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage('Você não tem permissão para acessar a página solicitada.');
            $this->_helper->redirector('index', 'index');
        } else {
            $view = $this->view->view = $this->_getParam('view');
            if ($view == "trash")
                $this->view->mensagens = $this->mapper->getMensagens('con_status = -1');
            elseif ($view == null || $view == "inbox")
                $this->view->mensagens = $this->mapper->getMensagens('con_status <> -1');
            else {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage('Página solicitada é inválida.');
                $this->_helper->redirector('index', 'index');
            }
        }
    }

    public function visualizarAction() {
        if (!isset(Zend_Registry::get('usuario')->u_tipo) || Zend_Registry::get('usuario')->u_tipo != Utils_ACL::_SUPERUSUARIO) {
            $this->feedback = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage('Você não tem permissão para acessar a página solicitada.');
            $this->_helper->redirector('index', 'index');
        } else {
            try {
                $m = $this->mapper->getMensagemById($this->id);
                if ($m->con_status == 1) {
                    $m->con_status = 0;
                }
                $this->mapper->save($m);
                $this->view->mensagem = $this->mapper->getMensagemById($this->id);
                $this->view->usuario = $this->mapper->getMensagemById($this->id, false, true);
            } catch (Exception $e) {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage('Ocorreu um erro. Detalhes: ' . $e->getMessage());
                $this->_helper->redirector('mensagens', 'contato');
            }
        }
    }

    public function removerAction() {
        if (!isset(Zend_Registry::get('usuario')->u_tipo) || Zend_Registry::get('usuario')->u_tipo != Utils_ACL::_SUPERUSUARIO) {
            $this->feedback = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
            $this->feedback->addMessage('error');
            $this->feedback->addMessage('Erro');
            $this->feedback->addMessage('Você não tem permissão para acessar a página solicitada.');
            $this->_helper->redirector('index', 'index');
        } else {
            try {
                $m = $this->mapper->getMensagemById($this->id);
                if ($m->con_status == -1) {
                    $this->mapper->remove($m);
                    $msg = "A mensagem {$m->con_assunto} foi removida com sucesso.";
                } else {
                    $m->con_status = -1;
                    $this->mapper->save($m);
                    $msg = "A mensagem {$m->con_assunto} foi movida para lixeira.";
                }
                $this->feedback->addMessage('success');
                $this->feedback->addMessage('Sucesso');
                $this->feedback->addMessage($msg);
            } catch (Exception $e) {
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage('Ocorreu um erro. Detalhes: ' . $e->getMessage());
            }
            $this->_helper->redirector('mensagens', 'contato');
        }
    }

}