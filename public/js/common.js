$(window).resize(function() {
    $('.main_content').css('height', ($(window).height()) - 60);
    $('.sidebar').css('height', ($(window).height()) - 40);
});

//* detect touch devices 
function is_touch_device() {
    return !!('ontouchstart' in window);
}
$(document).ready(function() {
    $(window).resize();

    $('.btn-voltar').html("Voltar");
    $('.btn-voltar').prepend('<i class="icon-chevron-left"></i> ');
    $('.btn-voltar').attr('onclick', "javascript:window.history.go(-1);");
    $('.btn-voltar').addClass('btn btn-large');

    $('.data').dateinput();
    $('.agenda-data').dateinput({trigger: true, min: -1});
    $(".datamask").inputmask("99/99/9999");

    $('.autosize').autosize();

    $(".has-validation").validator({
        position: 'bottom left',
        offset: [5, 0],
        messageClass: 'validator-error',
        message: '<div><em/></div>'
    }).attr('novalidate', 'novalidate');


    //* resize elements on window resize
    var lastWindowHeight = $(window).height();
    var lastWindowWidth = $(window).width();
    $(window).on("debouncedresize", function() {
        if ($(window).height() != lastWindowHeight || $(window).width() != lastWindowWidth) {
            lastWindowHeight = $(window).height();
            lastWindowWidth = $(window).width();
            if (!is_touch_device()) {
                $('.sidebar_switch').qtip('hide');
            }
        }
    });
    //* tooltips
    tips.init();

    //* external links
    external_links.init();
    //* accordion icons
    acc_icons.init();
    //* colorbox single
    colorbox_single.init();
    //* main menu mouseover
    nav_mouseover.init();
    //* top submenu
    submenu.init();

    
});

//* tooltips
tips = {
    init: function() {
        if (!is_touch_device()) {
            var shared = {
                style: {
                    classes: 'ui-tooltip-shadow ui-tooltip-tipsy'
                },
                show: {
                    delay: 100,
                    event: 'mouseenter focus'
                },
                hide: {
                    delay: 0
                }
            };
            if ($('.ttip_b').length) {
                $('.ttip_b').qtip($.extend({}, shared, {
                    position: {
                        my: 'top center',
                        at: 'bottom center',
                        viewport: $(window)
                    }
                }));
            }
            if ($('.ttip_t').length) {
                $('.ttip_t').qtip($.extend({}, shared, {
                    position: {
                        my: 'bottom center',
                        at: 'top center',
                        viewport: $(window)
                    }
                }));
            }
            if ($('.ttip_l').length) {
                $('.ttip_l').qtip($.extend({}, shared, {
                    position: {
                        my: 'right center',
                        at: 'left center',
                        viewport: $(window)
                    }
                }));
            }
            if ($('.ttip_r').length) {
                $('.ttip_r').qtip($.extend({}, shared, {
                    position: {
                        my: 'left center',
                        at: 'right center',
                        viewport: $(window)
                    }
                }));
            }
            ;
        }
    }
};

//* external links
external_links = {
    init: function() {
        $("a[href^='http']").not('.thumbnail>a,.ext_disabled').each(function() {
            $(this).attr('target', '_blank').addClass('external_link');
        })
    }
};

//* accordion icons
acc_icons = {
    init: function() {
        var accordions = $('.main_content .accordion');

        accordions.find('.accordion-group').each(function() {
            var acc_active = $(this).find('.accordion-body').filter('.in');
            acc_active.prev('.accordion-heading').find('.accordion-toggle').addClass('acc-in');
        });
        accordions.on('show', function(option) {
            $(this).find('.accordion-toggle').removeClass('acc-in');
            $(option.target).prev('.accordion-heading').find('.accordion-toggle').addClass('acc-in');
        });
        accordions.on('hide', function(option) {
            $(option.target).prev('.accordion-heading').find('.accordion-toggle').removeClass('acc-in');
        });
    }
};

//* main menu mouseover
nav_mouseover = {
    init: function() {
        $('header li.dropdown').mouseenter(function() {
            if ($('body').hasClass('menu_hover')) {
                $(this).addClass('navHover')
            }
        }).mouseleave(function() {
            if ($('body').hasClass('menu_hover')) {
                $(this).removeClass('navHover open')
            }
        });
    }
};

//* single image colorbox
colorbox_single = {
    init: function() {
        if ($('.cbox_single').length) {
            $('.cbox_single').colorbox({
                maxWidth: '80%',
                maxHeight: '80%',
                opacity: '0.2',
                fixed: true
            });
        }
    }
};

//* submenu
submenu = {
    init: function() {
        $('.dropdown-menu li').each(function() {
            var $this = $(this);
            if ($this.children('ul').length) {
                $this.addClass('sub-dropdown');
                $this.children('ul').addClass('sub-menu');
            }
        });

        $('.sub-dropdown').on('mouseenter', function() {
            $(this).addClass('active').children('ul').addClass('sub-open');
        }).on('mouseleave', function() {
            $(this).removeClass('active').children('ul').removeClass('sub-open');
        })

    }
};
