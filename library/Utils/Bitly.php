<?php

class Utils_Bitly extends Zend_Controller_Plugin_Abstract {

    private $cl_id;
    private $cl_secret;
    
    public function __construct($options = null) {
        $this->cl_id = $options['cl_id'];
        $this->cl_secret = $options['cl_secret'];
    }
    
    
    public function encurtar($url) {

        $servico_web = "http://api.bit.ly/";
        $version_API = "version=2.0.1";
        $usuario = "login=usuario"; //substitui pelo usuario do API
        $senha = "apiKey=R_senha"; //substitui pela senha do API
        
        $URL_longa = $url;

        $query_URL = "&longUrl=" . urlencode($URL_longa);
        $URL_consulta_API = $servico_web . "shorten?" . $version_API . "&" . $query_URL . "&" . $usuario . "&" . $senha;
        $resposta_API = json_decode(file_get_contents($URL_consulta_API), true);
        return $resposta_API["results"][$URL_longa]["shortUrl"];


    }
    
    public function twitter($url, $texto='EuDoador :: Doe sague você também: '){
        return 'http://twitter.com/home?status=' . urlencode($texto . $this->encurtar($url));
    }

}

