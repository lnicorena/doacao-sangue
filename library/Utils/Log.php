<?php

class Utils_Log extends Zend_Controller_Plugin_Abstract {

    const _INFO = '0';
    const _WARNING = '1';
    const _ERROR = '5';
    const _FB_API_ERROR = '5';
    const _FATAL_ERROR = '99';

    public function log($arrayContent, $nivel = self::_INFO) {

        $ip = $this->getIp();
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $mapper = new Doacao_Model_Mapper_Log();
        $log = new Doacao_Model_Log();
        $log->u_id = isset(Zend_Registry::get('usuario')->u_id) ? Zend_Registry::get('usuario')->u_id : null;
        $log->log_nivel = $nivel;
        $log->log_timestamp = "NOW()";

        $arrayContent['ip'] = $ip;
        $arrayContent['module'] = $request->getModuleName();
        $arrayContent['controller'] = $request->getControllerName();
        $arrayContent['action'] = $request->getActionName();
        $log->log_text = serialize($arrayContent);

        $mapper->save($log);
    }

    public function getIp() {
        $variables = array(
            'REMOTE_ADDR',
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'HTTP_X_COMING_FROM',
            'HTTP_COMING_FROM',
            'HTTP_CLIENT_IP'
        );

        $return = 'Desconhecido';

        foreach ($variables as $variable) {
            if (isset($_SERVER[$variable])) {
                $return = $_SERVER[$variable];
                break;
            }
        }

        return $return;
    }

}

