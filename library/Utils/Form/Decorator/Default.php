<?php

class Utils_Form_Decorator_Default extends Zend_Form {

    protected $_formDecorator = array('FormElements', 'Fieldset', 'Form');
    protected $_elementDecorator = array(
        'viewHelper',
        'label',
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'row-fluid')));
    protected $_multiCheckboxDecorator = array(
        //array( 'ViewHelper', array( 'label', array( 'class' => 'append' )  )),
        'Label',
        'Errors'
    );
    protected $_submitDecorator = array();

    public function __construct($options = null) {
        parent::__construct($options);

        $this->setAttrib('class', 'zend_form form-horizontal has-validation');
        $this->setDecorators($this->_formDecorator);
    }

    public function addDisplayGroup(array $elements, $name, $options = null) {
        parent::addDisplayGroup($elements, $name, $options);

        if ($name == 'botoes') {
            $this->getDisplayGroup($name)
                    ->addDecorators(array(
                        'FormElements',
                        array('HtmlTag', array('tag' => 'div', 'class' => 'controls'))
                    ))
                    ->removeDecorator('DtDdWrapper');
        } else {
            $this->getDisplayGroup($name)
                    ->addDecorators(array(
                        'FormElements',
                        array('HtmlTag', array('tag' => 'div', 'class' => 'row-fluid formSep'))
                    ))
                    ->removeDecorator('DtDdWrapper');
        }
    }

    public function addElement($element, $name = null, $options = null, $class = '') {
        if (!is_string($element)) {
            $element->setDisableLoadDefaultDecorators(true);

            if (( $element instanceof Zend_Form_Element_Submit )
                    || ( $element instanceof Zend_Form_Element_Button )
                    || ( $element instanceof Zend_Form_Element_Reset )) {
                $element->addDecorators($this->_submitDecorator)
                        ->removeDecorator('DtDdWrapper');
            } elseif ($element instanceof Zend_Form_Element_MultiCheckbox) {
                $element->addDecorators($this->_multiCheckboxDecorator)
                        ->setSeparator('')
                        ->removeDecorator('DtDdWrapper');
            } elseif ($element instanceof Zend_Form_Element_Radio) {
                $element->addDecorators($this->_elementDecorator)
                        ->setSeparator('');
                        //->removeDecorator('DtDdWrapper');
            } else {
                $element->addDecorators($this->_elementDecorator)
                        ->setAttrib('class', 'span12 ' . $element->getAttrib('class'));
            }

            if ($element->isRequired()) {
                $element->setAttrib('required', 'true');
            }
        }

        parent::addElement($element, $name, $options);

        return $this;
    }

    //função para montagem dos grupos (fieldsets)
    function montandoGrupo(array $nome, $valor) {
        $this->addDisplayGroup($nome, $valor);
    }

    //função para definir a posição de cada elemento do formulário
    function configurandoTamanho($nome, $valor, $append = false, $class = '') {
        if ($append) {
            $this->setElementDecorators(
                    array('ViewHelper',
                array('Description', array('tag' => 'span', 'class' => 'help-block')),
                'Label',
                'Errors',
                array(array('button' => 'HtmlTag'), array('tag' => 'button', 'type' => 'button',  'class' => 'btn ' . $class, 'placement' => Zend_Form_Decorator_Abstract::APPEND)),
                //array('HtmlTag', array('tag' => 'div', 'class' => 'input-append')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => $valor . ' input-append'))
                    ), array($nome)
            );
        } else {
            $this->setElementDecorators(
                    array('ViewHelper',
                array('Description', array('tag' => 'span', 'class' => 'help-block')),
                'Label',
                'Errors',
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => $valor))
                    ), array($nome)
            );
        }


        /* array(
          'viewHelper',
          array('Description', array('tag' => 'span', 'class' => 'help-block')),
          'label',

          array('Errors', array('HtmlTag' => 'label', 'class' => 'error')),
          array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => $valor))), array($nome) */
    }

    function removeElements($array) {
        foreach ($array as $name) {
            parent::removeElement($name);
        }
    }

}