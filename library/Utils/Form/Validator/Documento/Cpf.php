<?php

class Utils_Form_Validator_Documento_Cpf extends Utils_Form_Validator_Documento_Abstract {

    /**
     * Tamanho do Campo
     * @var int
     */
    protected $_size = 11;

    /**
     * Modificadores de Dígitos
     * @var array
     */
    protected $_modifiers = array(
        array(10, 9, 8, 7, 6, 5, 4, 3, 2),
        array(11, 10, 9, 8, 7, 6, 5, 4, 3, 2)
    );

}