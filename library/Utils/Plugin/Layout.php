<?php

class Utils_Plugin_Layout extends Zend_Controller_Plugin_Abstract {

    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request) {
        $sistema = array('cadastro');
        if (in_array($request->getModuleName(), $sistema)) {
            Zend_Layout::getMvcInstance()->setLayout('sistema');
        } elseif ($request->getModuleName() == 'site') {
            Zend_Layout::getMvcInstance()->setLayout('site');
        }
    }

}
