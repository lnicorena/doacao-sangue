<?php

class Utils_Plugin_ACL extends Zend_Controller_Plugin_Abstract {

    protected $_userRole = 'usuario';

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('EuDoador'));
        $acl = new Utils_ACL();

        if ($auth->hasIdentity()) {
            $acl->setPerfil($auth);
            if (!$acl->isAllowed($this->_userRole, $request->getModuleName() . ':' . $request->getControllerName(), $request->getActionName())) {
                $this->feedback = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage('Você não tem permissão para acessar a página solicitada.');

                return Zend_Controller_Action_HelperBroker::getStaticHelper('redirector')->setGotoUrl('/');
            }
        } else {
            if (!$acl->isAllowed($this->_userRole, $request->getModuleName() . ':' . $request->getControllerName(), $request->getActionName())) {

                $this->feedback = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
                $this->feedback->addMessage('error');
                $this->feedback->addMessage('Erro');
                $this->feedback->addMessage('Você não tem permissão para acessar a página solicitada.');
                return Zend_Controller_Action_HelperBroker::getStaticHelper('redirector')->setGotoUrl('/');
            }
        }
    }

}

?>