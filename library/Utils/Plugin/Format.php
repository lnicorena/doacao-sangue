<?php
class Utils_Plugin_Format extends Zend_Controller_Plugin_Abstract {
	protected $locale;
	
	/**
	 * Constantes definidas para melhor legibilidade do código.
	 * O prefixo NN_ indica que
	 * seu uso está relacionado ao método público e estático normalizarNome().
	 */
	const NN_PONTO = '\.';
	const NN_PONTO_ESPACO = '. ';
	const NN_ESPACO = ' ';
	const NN_REGEX_MULTIPLOS_ESPACOS = '\s+';
	const NN_REGEX_NUMERO_ROMANO = '^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$';
	public function init() {
		$this->locale = new Zend_Locale ();
		$this->locale->setDefault ( 'pt_BR' );
	}
	
	/**
	 * Normaliza o nome próprio dado, aplicando a capitalização correta de
	 * acordo
	 * com as regras e exceções definidas no código.
	 * POR UMA DECISÃO DE PROJETO, FORAM UTILIZADAS FUNÇÕES MULTIBYTE (MB_)
	 * SEMPRE
	 * QUE POSSÍVEL, PARA GARANTIR SUA USABILIDADE EM STRINGS UNICODE.
	 *
	 * @param string $nome
	 *        	O nome a ser normalizado
	 * @return string O nome devidamente normalizado
	 */
	public function normalizarNome($nome) {
		
		/*
		 * A primeira tarefa da normalização é lidar com partes do nome que
		 * porventura estejam abreviadas,considerando-se para tanto a existência
		 * de pontos finais (p. ex. JOÃO A. DA SILVA, onde "A." é uma parte
		 * abreviada). Dado que mais à frente dividiremos o nome em partes
		 * tomando em consideração o caracter de espaço (" "), precisamos
		 * garantir que haja um espaço após o ponto. Fazemos isso substituindo
		 * todas as ocorrências do ponto por uma sequência de ponto e espaço.
		 */
		$nome = mb_ereg_replace ( self::NN_PONTO, self::NN_PONTO_ESPACO, $nome );
		
		/*
		 * O procedimento anterior, ou mesmo a digitação errônea, podem ter
		 * introduzido espaços múltiplos entre as partes do nome, o que é
		 * totalmente indesejado. Para corrigir essa questão, utilizamos uma
		 * substituição baseada em expressão regular, a qual trocará todas as
		 * ocorrências de espaços múltiplos por espaços simples.
		 */
		$nome = mb_ereg_replace ( self::NN_REGEX_MULTIPLOS_ESPACOS, self::NN_ESPACO, $nome );
		
		/*
		 * Isso feito, podemos fazer a capitalização "bruta", deixando cada
		 * parte do nome com a primeira letra maiúscula e as demais minúsculas.
		 * Assim, JOÃO DA SILVA => João Da Silva.
		 */
		$nome = mb_convert_case ( $nome, MB_CASE_TITLE, mb_detect_encoding ( $nome ) );
		
		/*
		 * Nesse ponto, dividimos o nome em partes, para trabalhar com cada uma
		 * delas separadamente.
		 */
		$partesNome = mb_split ( self::NN_ESPACO, $nome );
		
		/*
		 * A seguir, são definidas as exceções à regra de capitalização. Como
		 * sabemos, alguns conectivos e preposições da língua portuguesa e de
		 * outras línguas jamais são utilizadas com a primeira letra maiúscula.
		 * Essa lista de exceções baseia-se na minha experiência pessoal, e pode
		 * ser adaptada, expandida ou mesmo reduzida conforme as necessidades de
		 * cada caso.
		 */
		$excecoes = array (
				'de',
				'di',
				'do',
				'da',
				'dos',
				'das',
				'dello',
				'della',
				'dalla',
				'dal',
				'del',
				'e',
				'em',
				'na',
				'no',
				'nas',
				'nos',
				'van',
				'von',
				'y' 
		);
		
		for($i = 0; $i < count ( $partesNome ); ++ $i) {
			
			/*
			 * Verificamos cada parte do nome contra a lista de exceções. Caso
			 * haja correspondência, a parte do nome em questão é convertida
			 * para letras minúsculas.
			 */
			foreach ( $excecoes as $excecao )
				if (mb_strtolower ( $partesNome [$i] ) == mb_strtolower ( $excecao ))
					$partesNome [$i] = $excecao;
				
				/*
			 * Uma situação rara em nomes de pessoas, mas bastante comum em
			 * nomes de logradouros, é a presença de numerais romanos, os quais,
			 * como é sabido, são utilizados em letras MAIÚSCULAS. No site
			 * http://htmlcoderhelper.com/how-do-you-match-only-valid-roman-numerals-with-a-regular-expression/,
			 * encontrei uma expressão regular para a identificação dos ditos
			 * numerais. Com isso, basta testar se há uma correspondência e, em
			 * caso positivo, passar a parte do nome para MAIÚSCULAS. Assim, o
			 * que antes era "Av. Papa João Xxiii" passa para "Av. Papa João
			 * XXIII".
			 */
			if (mb_ereg_match ( self::NN_REGEX_NUMERO_ROMANO, mb_strtoupper ( $partesNome [$i] ) ))
				$partesNome [$i] = mb_strtoupper ( $partesNome [$i] );
		}
		
		/*
		 * Finalmente, basta juntar novamente todas as partes do nome, colocando
		 * um espaço entre elas.
		 */
		return implode ( self::NN_ESPACO, $partesNome );
	}
	function array_column(array $input, $columnKey, $indexKey = null) {
		$result = array();
	
		if (null === $indexKey) {
			if (null === $columnKey) {
				// trigger_error('What are you doing? Use array_values() instead!', E_USER_NOTICE);
				$result = array_values($input);
			}
			else {
				foreach ($input as $row) {
					$result[] = $row[$columnKey];
				}
			}
		}
		else {
			if (null === $columnKey) {
				foreach ($input as $row) {
					$result[$row[$indexKey]] = $row;
				}
			}
			else {
				foreach ($input as $row) {
					$result[$row[$indexKey]] = $row[$columnKey];
				}
			}
		}
	
		return $result;
	}
	public function unformatNumber($number) {
		$number = Zend_Locale_Format::getNumber ( $number, array (
				'locale' => $this->locale,
				'precision' => 2 
		) );
		return $number;
	}
	public function formatNumber($number) {
                if($number == 0) return "0,00";
		$number = Zend_Locale_Format::toNumber ( $number, array (
				'locale' => $this->locale,
				'precision' => 2 
		) );
		return $number;
	}
	public function token($valor) {
		$a = str_replace ( array (
				'[{',
				'}]',
				'null' 
		), '', $valor );
		$b = substr ( $a, 0, - 1 );
		$c = explode ( ',', $b );
		$d = array_unique ( $c );
		$e = implode ( ",", $d );
		return $e;
	}
	function diferencaHoras($dhini, $dhfim) {
		$datetime1 = new DateTime ( $dhini );
		$datetime2 = new DateTime ( $dhfim );
		$interval = $datetime1->diff ( $datetime2 );
		return $interval->format ( '%H:%I:%S' );
	}
	public function geraTimestamp($data) {
		$partes = explode ( '/', $data );
		return mktime ( 0, 0, 0, $partes [1], $partes [0], $partes [2] );
	}
	public function diferencaDatas($data_inicial, $data_final) {
		// Usa a função criada e pega o timestamp das duas datas:
		$time_inicial = $this->geraTimestamp ( $data_inicial );
		$time_final = $this->geraTimestamp ( $data_final );
		
		// Calcula a diferença de segundos entre as duas datas:
		$diferenca = $time_final - $time_inicial; // 19522800 segundos
		                                          // Calcula a diferença de dias
		$dias = ( int ) floor ( $diferenca / (60 * 60 * 24) ); // 225 dias
		
		return $dias;
	}
	public function NewMonthDate($n, $y, $m, $d, $t) {
		$mm = 0;
		for($i = 1; $i <= $n; $i ++) {
			if ($t == "+") {
				$pmonth = $m + $i;
			}
			if ($t == "-") {
				$pmonth = $m - $i;
			}
			
			$pyear = $y;
			$aa = 1;
			
			// ///////////////this will show lastmonth//////////////
			$lastmonth = mktime ( 0, 0, 0, $pmonth, $aa, $pyear );
			
			$smonth = date ( "m", $lastmonth );
			$syear = date ( "Y", $lastmonth );
			$ndays = date ( "t", $lastmonth );
			$valarray = array (
					$smonth,
					$syear,
					$ndays 
			);
			
			$mm = $mm + $ndays;
		}
		
		$basedate = "$pyear-$m-$d";
		$date2 = strtotime ( "$basedate $t$mm days" );
		
		$date1 = date ( "Y-m-d H:i:s", $date2 );
		return $date1;
	}
	public function formatTimestamp($timestamp, $r = 0) {
		$text = "";
		if (empty ( $timestamp )) {
			return $text;
		} else {
			$format = explode ( " ", $timestamp );
			$fd = explode ( "-", $format [0] );
			$ft = explode ( ":", $format [1] );
			
			$dateFormat = $fd [2] . "/" . $fd [1] . "/" . $fd [0];
			$timeFormat = $ft [0] . ":" . $ft [1];
			$s = explode ( ".", $ft [2] );
			if ($r == 1) {
				$text = $dateFormat;
			} elseif ($r == 2) {
				$text = $timeFormat;
			} elseif ($r == 3) {
				$text = $ft [0] . ":" . $ft [1] . ":" . $s [0];
			} elseif ($r == 4) {
				$text = $format [0] . ' ' . $timeFormat . ":" . $s [0];
			} elseif ($r == 5) {
				$text = $fd [2];
			} elseif ($r == 6) {
				$text = $fd [1];
			} elseif ($r == 7) {
				$text = $fd [0];
			} else {
				$text = $dateFormat . ' ' . $timeFormat;
			}
			return $text;
		}
	}
	public function formatTime($time, $r = 0) {
		$text = "";
		if (empty ( $time )) {
			return $text;
		} else {
			$ft = explode ( ":", $time );
			
			$timeFormat = $ft [0] . ":" . $ft [1];
			
			if ($ft [2]) {
				$ft [2] = explode ( '.', $ft [2] );
				$timeFormat .= ":" . $ft [2] [0];
			}
			
			$text = $timeFormat;
			
			return $text;
		}
	}
	function formatFilesize($a_bytes) {
		if ($a_bytes < 1024) {
			return $a_bytes . ' B';
		} elseif ($a_bytes < 1048576) {
			return round ( $a_bytes / 1024, 2 ) . ' KB';
		} elseif ($a_bytes < 1073741824) {
			return round ( $a_bytes / 1048576, 2 ) . ' MB';
		} elseif ($a_bytes < 1099511627776) {
			return round ( $a_bytes / 1073741824, 2 ) . ' GB';
		} elseif ($a_bytes < 1125899906842624) {
			return round ( $a_bytes / 1099511627776, 2 ) . ' TB';
		} elseif ($a_bytes < 1152921504606846976) {
			return round ( $a_bytes / 1125899906842624, 2 ) . ' PB';
		} elseif ($a_bytes < 1180591620717411303424) {
			return round ( $a_bytes / 1152921504606846976, 2 ) . ' EB';
		} elseif ($a_bytes < 1208925819614629174706176) {
			return round ( $a_bytes / 1180591620717411303424, 2 ) . ' ZB';
		} else {
			return round ( $a_bytes / 1208925819614629174706176, 2 ) . ' YB';
		}
	}
	
	// formata data que vem do BD.
	public function formatDate($date) {
		if (empty ( $date )) {
			$text = "";
			return $text;
		} else {
			$format = explode ( "-", $date );
			if (count ( $format ) > 1) {
				$dateFormat = $format [2] . "/" . $format [1] . "/" . $format [0];
				return $dateFormat;
			} else
				return $date;
		}
	}
	
	// formata data que vai pro BD.
	public function unformatDate($date) {
		if (empty ( $date )) {
			$text = NULL;
			return $text;
		} else {
			$format = explode ( "/", $date );
			if (count ( $format ) > 1) {
				$dateFormat = $format [2] . "-" . $format [1] . "-" . $format [0];
				return $dateFormat;
			} else
				return $date;
		}
	}
	public function soNumeros($valor) {
		return preg_replace ( "/[^0-9]/", "", $valor );
	}
	public function maiuscula($string) {
		return strtoupper ( strtr ( $string, "áéíóúâêôãõàèìòùç", "ÁÉÍÓÚÂÊÔÃÕÀÈÌÒÙÇ" ) );
	}
	public function minuscula($string) {
		return strtolower ( strtr ( $string, "ÁÉÍÓÚÂÊÔÃÕÀÈÌÒÙÇ", "áéíóúâêôãõàèìòùç" ) );
	}
	public function SemAcentos($string = "", $mesma = 1) {
		if ($string != "") {
			$com_acento = "à á â ã ä è é ê ë ì í î ï ò ó ô õ ö ù ú û ü À Á Â Ã Ä È É Ê Ë Ì Í Î Ò Ó Ô Õ Ö Ù Ú Û Ü ç Ç ñ Ñ";
			$sem_acento = "a a a a a e e e e i i i i o o o o o u u u u A A A A A E E E E I I I O O O O O U U U U c C n N";
			$c = explode ( ' ', $com_acento );
			$s = explode ( ' ', $sem_acento );
			
			$i = 0;
			foreach ( $c as $letra ) {
				if (ereg ( $letra, $string )) {
					$pattern [] = $letra;
					$replacement [] = $s [$i];
				}
				$i = $i + 1;
			}
			
			if (isset ( $pattern )) {
				$i = 0;
				foreach ( $pattern as $letra ) {
					$string = eregi_replace ( $letra, $replacement [$i], $string );
					$i = $i + 1;
				}
				return $string;
			}
			if ($mesma != 0) {
				return $string;
			}
		}
		return "";
	}
	public function normaliza($string) {
	}
	public function pluralCount($x, $singular, $plural, $nenhuma) {
		if ($x == 0) {
			return $nenhuma;
		} elseif ($x == 1 || $x == - 1) {
			return $x . " " . $singular;
		} elseif ($x > 1 || $x < - 1) {
			return $x . " " . $plural;
		} else {
			return $x;
		}
	}
	public function formatarCPF_CNPJ($campo, $formatado = true) {
		// retira formato
		$codigoLimpo = @ereg_replace ( "[' '-./ t]", '', $campo );
		// pega o tamanho da string menos os digitos verificadores
		$tamanho = (strlen ( $codigoLimpo ) - 2);
		// verifica se o tamanho do código informado é válido
		if ($tamanho != 9 && $tamanho != 12) {
			return false;
		}
		
		if ($formatado) {
			// seleciona a máscara para cpf ou cnpj
			$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##';
			
			$indice = - 1;
			for($i = 0; $i < strlen ( $mascara ); $i ++) {
				if ($mascara [$i] == '#')
					$mascara [$i] = $codigoLimpo [++ $indice];
			}
			// retorna o campo formatado
			$retorno = $mascara;
		} else {
			// se não quer formatado, retorna o campo limpo
			$retorno = $codigoLimpo;
		}
		
		return $retorno;
	}
	
	/*
	 * @str String para fazer truncate @length quantos caracteres irá ter a
	 * string depois do truncate @trailing O que irá aparecer no final da string
	 * após fazer o truncate, neste caso por defeito vem "..." @return String
	 */
	function truncate($str, $length = 10, $trailing = '...') {
		$length -= mb_strlen ( $trailing );
		
		if (mb_strlen ( $str ) > $length) {
			return mb_substr ( $str, 0, $length ) . $trailing;
		} else {
			$res = $str;
		}
		return $res;
	}
	public function retira_acentos($texto) {
		$array1 = array (
				"á",
				"à",
				"â",
				"ã",
				"ä",
				"é",
				"è",
				"ê",
				"ë",
				"í",
				"ì",
				"î",
				"ï",
				"ó",
				"ò",
				"ô",
				"õ",
				"ö",
				"ú",
				"ù",
				"û",
				"ü",
				"ç",
				"Á",
				"À",
				"Â",
				"Ã",
				"Ä",
				"É",
				"È",
				"Ê",
				"Ë",
				"Í",
				"Ì",
				"Î",
				"Ï",
				"Ó",
				"Ò",
				"Ô",
				"Õ",
				"Ö",
				"Ú",
				"Ù",
				"Û",
				"Ü",
				"Ç" 
		);
		$array2 = array (
				"a",
				"a",
				"a",
				"a",
				"a",
				"e",
				"e",
				"e",
				"e",
				"i",
				"i",
				"i",
				"i",
				"o",
				"o",
				"o",
				"o",
				"o",
				"u",
				"u",
				"u",
				"u",
				"c",
				"A",
				"A",
				"A",
				"A",
				"A",
				"E",
				"E",
				"E",
				"E",
				"I",
				"I",
				"I",
				"I",
				"O",
				"O",
				"O",
				"O",
				"O",
				"U",
				"U",
				"U",
				"U",
				"C" 
		);
		return str_replace ( $array1, $array2, $texto );
	}
	function get_mime_type($file) {
		
		// our list of mime types
		$mime_types = array (
				"pdf" => "application/pdf",
				"exe" => "application/octet-stream",
				"zip" => "application/zip",
				"docx" => "application/msword",
				"doc" => "application/msword",
				"xls" => "application/vnd.ms-excel",
				"ppt" => "application/vnd.ms-powerpoint",
				"gif" => "image/gif",
				"png" => "image/png",
				"jpeg" => "image/jpg",
				"jpg" => "image/jpg",
				"mp3" => "audio/mpeg",
				"wav" => "audio/x-wav",
				"mpeg" => "video/mpeg",
				"mpg" => "video/mpeg",
				"mpe" => "video/mpeg",
				"mov" => "video/quicktime",
				"avi" => "video/x-msvideo",
				"3gp" => "video/3gpp",
				"css" => "text/css",
				"jsc" => "application/javascript",
				"js" => "application/javascript",
				"php" => "text/html",
				"htm" => "text/html",
				"html" => "text/html" 
		);
		
		$extension = strtolower ( end ( explode ( '.', $file ) ) );
		
		return $mime_types [$extension];
	}
	
	function slugify($text) {
		// replace non letter or digits by -
		$text = preg_replace ( '~[^\\pL\d]+~u', '-', $text );	
		// trim
		$text = trim ( $text, '-' );
		// transliterate
		$text = iconv ( 'utf-8', 'us-ascii//TRANSLIT', $text );
		// lowercase
		$text = strtolower ( $text );
		// remove unwanted characters
		$text = preg_replace ( '~[^-\w]+~', '', $text );
		
		if (empty ( $text )) {
			return 'n-a';
		}
		
		return $text;
	}
	
	function valorPorExtenso($valor=0, $complemento=true) {
		$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
		$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
	
		$c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
		$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
		$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
		$u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
	
		$z=0;
		$rt=false;
	
		$valor = number_format($this->unformatNumber($valor), 2, ".", ".");
		$inteiro = explode(".", $valor);
		for($i=0;$i<count($inteiro);$i++)
		for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
			$inteiro[$i] = "0".$inteiro[$i];
	
			// $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
			$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
			for ($i=0;$i<count($inteiro);$i++) {
			$valor = $inteiro[$i];
			$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
			$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
			$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
	
			$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
			$t = count($inteiro)-1-$i;
			if ($complemento == true) {
			$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
			if ($valor == "000")$z++; elseif ($z > 0) $z--;
			if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
			}
			if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
			}
	
			return($rt ? $rt : "zero");
	}
	
	function dataPorExtenso($data = false, $semana = false) {
		if ($data) {
			$mes = date('m', strtotime($data));
		} else {
			$mes = date('m');
			$data = date('Y-m-d');
		}
		
		$meses = array (
			'01' => 'Janeiro',
			'02' => 'Fevereiro',
			'03' => 'Março',
			'04' => 'Abril',
			'05' => 'Maio',
			'06' => 'Junho',
			'07' => 'Julho',
			'08' => 'Agosto',
			'09' => 'Setembro',
			'10' => 'Outubro',
			'11' => 'Novembro',
			'12' => 'Dezembro'
		);
		
		$dias = array (
			0 => 'Domingo',
			1 => 'Segunda-feira',
			2 => 'Terça-feira',
			3 => 'Quarta-feira',
			4 => 'Quinta-feira',
			5 => 'Sexta-feira',
			6 => 'Sábado'
		);
		
		if ($semana) {
			return $dias[date('w', strtotime($data))] . ', ' . date('d', strtotime($data)) . ' de ' . $meses[$mes] . ' de ' . date('Y', strtotime($data));
		} else {
			return date('d', strtotime($data)) . ' de ' . $meses[$mes] . ' de ' . date('Y', strtotime($data));
		}
		
		
	}
}

?>
