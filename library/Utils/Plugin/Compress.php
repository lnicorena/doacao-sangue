<?php

class Utils_Plugin_Compress extends Zend_Controller_Plugin_Abstract {

    public function postDispatch(Zend_Controller_Request_Abstract $request) {
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();

        if ($controller != 'arquivo') {
            $response = $this->getResponse();
            $buffer = $response->getBody();

            /* remove os comentarios */
            $buffer = preg_replace('#<!--.*?-->#s', '', $buffer);
            $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
            /* remove tabs, espaços, newlines, etc. */
            $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '" "', '  ', '    ', '    '), '', $buffer);

            $response->setBody($buffer);
        }
    }

}