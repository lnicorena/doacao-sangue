<?php

class Utils_Plugin_Notifica extends Zend_Controller_Plugin_Abstract {

    private $mail;

    public function __construct($options = null) {
        $this->mail = new Utils_Mailer_PHPMailer(true);
        if (is_array($options) && $options != null) {
            if ($options ['smtp']) {
                $this->mail->IsSMTP();
                $this->mail->SMTPAuth = $options ['smtpauth'];
                $this->mail->SMTPSecure = $options ['smtpsecure'];
                $this->mail->Host = $options ['host'];
                $this->mail->Port = $options ['port'];
                $this->mail->Username = $options ['username'];
                $this->mail->Password = $options ['password'];
                $this->mail->SetFrom($options ['from_mail'], $options ['from_nome']);
            } else {
                $this->mail->IsSendmail();
            }

            $this->mail->SetFrom($options ['from_mail'], $options ['from_nome']);
        }
    }

    public function email($email, $nome, $titulo, $msg) {
        try {
            $this->mail->AddAddress($email, $nome);

            $this->mail->Subject = sprintf('=?%s?%s?%s?=', 'UTF-8', 'B', base64_encode('EuDoador :: ' . $titulo)); //=?charset?método?texto codificado?=
            $this->mail->AltBody = 'Para visualizar esta mensagem utilize um visualizador de emails compatível com HTML!';
            $this->mail->MsgHTML($msg);
            $this->mail->Send();
            return true;
        } catch (phpmailerException $e) {
            //print $e->errorMessage(); // Pretty error messages from PHPMailer
            return false;
        } catch (Exception $e) {
            //print $e->getMessage(); // Boring error messages from anything else!
            return false;
        }
    }

}