<?php

class Utils_ACL extends Zend_Acl {

    const _SUPERUSUARIO = '777';
    const _USUARIO = '1';

    public function __construct() {
        $this->addRole(new Zend_Acl_Role('usuario'));

        $this->add(new Zend_Acl_Resource('default:ajax'))
                ->add(new Zend_Acl_Resource('default:app'))
                ->add(new Zend_Acl_Resource('default:contato'))
                ->add(new Zend_Acl_Resource('default:error'))
                ->add(new Zend_Acl_Resource('default:index'))
                ->add(new Zend_Acl_Resource('default:solicitacao'))
                ->add(new Zend_Acl_Resource('default:unidade'))
                ->add(new Zend_Acl_Resource('default:usuario'))
                
                ->add(new Zend_Acl_Resource('default:log'));


        $this->allow('usuario');
        $this->deny('usuario', 'default:log');
    }

    public function setPerfil($auth) {
        if ($auth instanceof Zend_Auth) {
            $perfil = ($auth->getIdentity()->u_tipo != '') ? $auth->getIdentity()->u_tipo : null;

            if ($perfil == self::_SUPERUSUARIO) {
                $this->allow('usuario', 'default:log');
            }
            
        }
    }

}
